/* 
Author: Subhash Shipu

 */
var App = angular.module('myApp', ['ngRoute','ngSanitize']).
  config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	  
	$routeProvider.
		when('/',{ 
					templateUrl:BASE_URL+'front/home',
					controller: 'indexCtrl'
				}
			).
      	when('/about', {
				templateUrl:BASE_URL+'front/about_us',
				controller: 'aboutCtrl'
			
			}).
      	when('/time-table', { templateUrl:BASE_URL+'front/time_table'}).
      	when('/staff', { templateUrl:BASE_URL+'front/staff'}).
      	when('/contact-us', { templateUrl:BASE_URL+'front/contact_us'}).
      	when('/register', { templateUrl:BASE_URL+'front/register'}).
      	when('/notice-board', { templateUrl:BASE_URL+'front/notice_board'}).
		otherwise({redirectTo:'/'});
		
		$locationProvider.html5Mode({
		  enabled: true,
		  requireBase: false
		});
		
	}]);
  
 App.controller('NavigationCtrl', ['$scope', '$location', function ($scope, $location) {
    $scope.home = "Home";
    $scope.about = "About Us";
    $scope.time_table = "Time Table";
    $scope.staff = "Faculty";
    $scope.notice_board = "Notice board";
    $scope.contact_us = "Contact Us";
	$scope.isCurrentPath = function (path) {
		return $location.path() == path;
    };
	
}]);

// Index Controller

 App.controller('indexCtrl', ['$scope', '$location', '$http' , function ($scope, $location, $http) {
		
	$http.get(BASE_URL+'front/get_staff')
	  .then(function(resp) {
		$scope.teachers = resp.data; 
	});	 
}]);

 App.controller('aboutCtrl', ['$scope', '$location', '$http' , function ($scope, $location, $http) {
		
	
}]);

// staff Controller
 App.controller('staffCtrl', ['$scope', '$location', '$http' , function ($scope, $location, $http) {
		
	$http.get(BASE_URL+'front/get_staff')
	  .then(function(resp) {
		$scope.teachers = resp.data; 
	});
		 
}]);

// Timetable Controller
 App.controller('timetableCtrl', ['$scope', '$http', '$sce' , function ($scope, $http, $sce) {
	
	$http({
			url: BASE_URL+"front/get_timetable",
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data:  {s:"ok"},
		}).then(function (response) {
			$scope.timetabledata = $sce.trustAsHtml(response.data.html);
			$scope.title = response.data.title;
		},function (error){
			console.log(error, 'can not get data.');
		});	
}]);

// noticeBoardCtrl Controller
 App.controller('noticeBoardCtrl', ['$scope', '$http', '$sce' , function ($scope, $http, $sce) {
	
	$http({
			url: BASE_URL+"front/get_noticeboard_data",
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data:  {s:"ok"},
		}).then(function (response) {
			$scope.coaching_institite = $sce.trustAsHtml(response.data.coaching_institite);
			$scope.faq = $sce.trustAsHtml(response.data.faq);
			$scope.pdf = response.data.pdf;
			
		},function (error){
			console.log(error, 'can not get data.');
		});	
}]);

// Register Controller
 App.controller('registerCtrl', ['$scope', '$location', '$http' , function ($scope, $location, $http, $window) {
		
		$scope.registerdata = {};
		$scope.submitForm = function () 
		{
			var fd = new FormData();
			if($scope.fileArray !== undefined){
				for (i=0; i< $scope.fileArray.length; i++) {
					fd.append("profile_image", $scope.fileArray[i]);
				}
			};
			fd.append("name", $scope.registerdata.name);
			fd.append("email", $scope.registerdata.email);
			fd.append("dob", $scope.registerdata.dob);
			fd.append("phone", $scope.registerdata.phone);
			fd.append("father_phone", $scope.registerdata.father_phone);
			fd.append("father_occupation", $scope.registerdata.father_occupation);
			fd.append("address", $scope.registerdata.address);
			fd.append("school", $scope.registerdata.school);
			fd.append("classes", $scope.registerdata.classes);
			fd.append("subjects", $scope.registerdata.subjects);
			fd.append("s", 'OK');
			
			$http({
					url: BASE_URL+"front/register",
					method: "POST",
					headers: {'Content-Type': undefined},
					data:  fd,
				}).then(function (resp) {
					var status = resp.data.status;
					$('.result_container').removeClass('alert-danger alert-success');
					if(status=="error")
					{
						$('.result_data').html(resp.data.html);
						$('.result_container').addClass('alert-danger');
						$('.result_container').removeClass('d-none');
					}
					else
					{
						$('.result_data').html(resp.data.html);
						$('.result_container').addClass('alert-success');
						$('.result_container').removeClass('d-none');
					}
				});
        };

}]);

App.directive("selectNgFiles", function() {
	
  return {
    require: "ngModel",
    link: function postLink(scope,elem,attrs,ngModel) {
      elem.on("change", function(e) {
		
        var files = elem[0].files;
		ngModel.$setViewValue(files);
      })
    }
  }
});

App.run(['$rootScope',function($rootScope){

    $rootScope.stateIsLoading = false;
    $rootScope.$on('$routeChangeStart', function() {
        $rootScope.stateIsLoading = true;
    });
    $rootScope.$on('$routeChangeSuccess', function() {
        $rootScope.stateIsLoading = false;
    });
    $rootScope.$on('$routeChangeError', function() {
        //catch error
    });

}]);


//countact us

App.controller('contactCtrl', ['$scope', '$location', '$http' , function ($scope, $location, $http) {	

	$http.get(BASE_URL+'front/get_contact_us')	
	  .then(function(resp) {		
	  
		$scope.address = resp.data.address;
		$scope.email = resp.data.email;
		$scope.phone = resp.data.phone;
	  });		
	  $scope.contactdata = {};
		
		$scope.submitForm = function () 
		{
			var fd = new FormData();
			fd.append("name", $scope.contactdata.name);
			fd.append("email", $scope.contactdata.email);
			fd.append("message", $scope.contactdata.message);
			fd.append("subject", $scope.contactdata.subject);
			
			
			$http({
					url: BASE_URL+"ajax/contact_form",
					method: "POST",
					headers: {'Content-Type': undefined},
					data:  fd,
				}).then(function (resp) {
					var status = resp.data.status;
					$('.result_container').removeClass('alert-danger alert-success');
					if(status=="error")
					{
						$('.result_data').html(resp.data.html);
						$('.result_container').addClass('alert-danger');
						$('.result_container').removeClass('d-none');
					}
					else
					{
						$('.result_data').html(resp.data.html);
						$('.result_container').addClass('alert-success');
						$('.result_container').removeClass('d-none');
					}
				});
        };
  }]);
//social link
App.controller('sociallinkCtrl', ['$scope', '$location', '$http' , function ($scope, $location, $http) {	
	$http.get(BASE_URL+'front/get_social_links')	
	  .then(function(resp) {		
		
		$scope.facebook = resp.data.facebook;
		$scope.google_plus = resp.data.google_plus;
		$scope.linkedin = resp.data.linkedin;
		$scope.twitter = resp.data.twitter;
	  });		
  }]);
// simple js

$(document).on('focus','input,select',function(){
	$('.error_container').addClass('d-none');
})



