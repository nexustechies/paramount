// Author : Subhash Shipu
// Email: Provider.nexus@gmail.com
// Skype : provider.nexus@gmail.com

$(document).ready(function() 
	{		var e = $("#datatable");
			e.dataTable(		{
			   searching: false,
			   "paging":   false,
			   "info":     false,
				"order": []			   
			});
	});

function showLoader()
	{
		$(document).find('#on_load_loader').removeClass('hide');
	}
	
function refreshmodal()	
	{		
		$('#MyModal').find('.modal-title').html('');
		$('#MyModal').find('.modal-body').html('');
		$('#MyModal').find('.modal-footer span').html('');	
	}
	
function modalbox(result)
	{
		refreshmodal()
		if(result.type == "modal" )
		{
			$('#MyModal').find('.modal-title').html(result.title);
			$('#MyModal').find('.modal-body').html(result.html);
			$('#MyModal').find('.modal-footer span').html(result.button);
		}
		else 
		{
			if(result.status == "success" )			
			{				
				$('#MyModal').find('.modal-title').html('Success');
				$('#MyModal').find('.modal-body').html(result.html);
			}
			else
			{
				$('#MyModal').find('.modal-title').html('Error');
				$('#MyModal').find('.modal-body').html(result.html);
			}
		}
		$('#MyModal').modal();
		
		$('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
	}	
	
function CallAjax(PATH,data)
	{
		var result;
		var BASE_URL = $(document).find('#base_url').val();
		$.ajax(
		{
			type: "POST",
			url: BASE_URL+PATH,
			data: data,
			dataType: "json",
			async:false,
			success: function(resp)
			{
				result = resp;
			}
		});
		return result;
	}
	
function swtalert(result)
	{
		if(result.status == "success" )
		{
			swal("Success", result.html, "success");
		}
		else
		{
			swal("Error", result.html, "error");
		}
		
	}	
	
$('.change_status').on('change.bootstrapSwitch', function(e) {
		
		var id = $(this).val();
		var action = $(this).prop('checked');
		var value = action == true ? 1 : 0;
		var path = $(this).attr('module');
		var data = "value="+value+'&id='+id;
		var result = CallAjax(path,data);	
		if(result.status)
		{	
			swtalert(result);
		}
});

// validation on number
$(document).on('keyup','input[type="tel"]',function(e)
	{
		e.preventDefault();
		var mobNum = $(this).val();
		var filter = /^\d*(?:\.\d{1,2})?$/;

		  if (filter.test(mobNum)) 
			{
				if(mobNum.length >= 10 && mobNum.length <= 12 )
					{
					
					  $(this).parent('div').removeClass('has-error');
					  $(this).parent('div').find('.help-block').html('');
					}
					else 
					{
						if(mobNum.length > 10) { $(this).val(mobNum.substr(0, 12)); return false ; }
						
						$(this).parent('div').addClass('has-error');
						$(this).parent('div').find('.help-block').html('Please put a valid number');
						return false;
					}
			}
			else 
			{
				$(this).val('');
			  $(this).parent('div').addClass('has-error');
			  $(this).parent('div').find('.help-block').html('Not a valid number');
			  return false;
			}
	});
	