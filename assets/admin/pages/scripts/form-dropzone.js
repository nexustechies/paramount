var path = $(document).find('#base_url').val();
var type = $(document).find('#base_url').val();

Dropzone.options.myAwesomeDropzone = 
{
    url: path+"users/ajax/DoUploadSession",
	dataType: 'json',
    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 3,
    maxFiles: 3,
    init: function () {
        var myDropzone = this;
		this.on("addedfile", function(file) 
		{
			// Create the remove button
			var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>");
			
			// Capture the Dropzone instance as closure.
			var _this = this;

			// Listen to the click event
			removeButton.addEventListener("click", function(e) 
			{
			  // Make sure the button click doesn't submit the form:
			  e.preventDefault();
			  e.stopPropagation();
			_this.removeFile(file);
			  // Removen  the file preview.
			});
			
			// Add the button to the file preview element.
			file.previewElement.appendChild(removeButton);
		});
		
		$('#submit_form').on("click", function() 
		{
            myDropzone.processQueue();
		});
       this.on("success", function(file, response) 
		{
			var obj = jQuery.parseJSON(response)
			swtalert(obj);
        });
        this.on("completemultiple", function(files) 
		{
           
        });
    },

};