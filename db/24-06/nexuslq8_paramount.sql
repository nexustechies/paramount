-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 24, 2019 at 01:55 PM
-- Server version: 5.6.41-84.1-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nexuslq8_paramount`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `email`, `password`) VALUES
(1, 'Paramount', 'admin', 'e6e061838856bf47e1de730719fb2609');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classes`
--

CREATE TABLE `tbl_classes` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_classes`
--

INSERT INTO `tbl_classes` (`id`, `title`, `value`) VALUES
(1, '10TH', 0),
(2, '11TH', 0),
(3, '12TH', 0),
(4, '9TH', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payments`
--

CREATE TABLE `tbl_payments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `paid_amount` varchar(255) NOT NULL,
  `pending_amount` varchar(255) NOT NULL,
  `paid_on` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `created_on` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payments`
--

INSERT INTO `tbl_payments` (`id`, `user_id`, `subject`, `total_amount`, `paid_amount`, `pending_amount`, `paid_on`, `description`, `created_on`) VALUES
(1, 1, '1,5', '3000', '1500', '1500', '2019-06-25', '', 1561369805);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `type`, `value`) VALUES
(1, 'result_per_page', '100'),
(2, 'social_links', 'a:4:{s:8:\"facebook\";s:25:\"https://www.facebook.com/\";s:11:\"google_plus\";s:20:\"https://twitter.com/\";s:8:\"linkedin\";s:20:\"https://twitter.com/\";s:7:\"twitter\";s:20:\"https://twitter.com/\";}'),
(3, 'contact_us', 'a:3:{s:5:\"phone\";s:14:\"+911995-259888\";s:5:\"email\";s:29:\"paramountkishtwar99@gmail.com\";s:7:\"address\";s:45:\"Opposite Govt. Girls Hr. Sec. School Kishtwar\";}'),
(4, 'coaching_institite', '<table id=\"tablepress-2\" class=\"tablepress tablepress-id-2\">\r\n<tbody class=\"row-hover\">\r\n<tr class=\"row-1 odd\">\r\n<td class=\"column-1\">MIDDLE CLASSES</td><td class=\"column-2\">8TH CLASS</td>\r\n</tr>\r\n<tr class=\"row-2 even\">\r\n<td class=\"column-1\">SECONDARY CLASSES</td><td class=\"column-2\">9TH &amp; 10TH CLASS</td>\r\n</tr>\r\n<tr class=\"row-3 odd\">\r\n<td class=\"column-1\">HIGHER SECONDARY CLASSES</td><td class=\"column-2\">H.SP-I (11TH) &amp; HSP-II (12TH) CLASS</td>\r\n</tr>\r\n<tr class=\"row-4 even\">\r\n<td class=\"column-1\">COLLEGE CLASSES</td><td class=\"column-2\">B.SC/B.A.:- Part-1; 2; &amp;3 CLASS</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(5, 'faq', '<ul>\r\n<li>Students Opt Tuition subject wise.</li>\r\n<li>Fees is also subject wise (Full Course/Monthly—- Both options)</li>\r\n<li>Fees is paid by students in options.</li>\r\n<li>Fees is taken by different cashiers which is later transferred by other to bank or to the teacher.</li>\r\n<li>Office employee users should be controlled by admin.</li>\r\n<li>Main website control like upload any notice etc by admin.</li>\r\n<li>Payment account for employees, rent building, rent electricity, other expenses and teachers etc.</li>\r\n<li>Subject wise/ class-wise list of students for each teacher-wise ( 1-Subject taught by many teachers).</li>\r\n</ul>'),
(6, 'pdf', 'img_1560174219_PIEAdmForm.docx');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `father_phone` varchar(255) NOT NULL,
  `father_occupation` varchar(255) NOT NULL,
  `address` longtext NOT NULL,
  `school` varchar(255) NOT NULL,
  `class` int(11) NOT NULL DEFAULT '0',
  `subjects` longtext NOT NULL,
  `payment_pending` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`id`, `name`, `email`, `dob`, `phone`, `profile_image`, `father_name`, `father_phone`, `father_occupation`, `address`, `school`, `class`, `subjects`, `payment_pending`, `status`, `created_on`) VALUES
(1, 'ankit', 'ankitkumar.nexus@gmail.com', '2016-01-13', '1234556778', '', 'test', '44234424243', 'fsdfvvfdv', 'xasxasx', 'adcd', 3, '1,3,5', 1, 1, '1561361532'),
(2, 'ankit', 'ankit.nexus@gmail.com', '2019-06-02', '1234556778', '', 'test', '44234424243', 'fsdfvvfdv', 'xasxasx', 'adcd', 2, '1', 0, 1, '1561371184'),
(3, 'ankit', 'ankitkumar.nexus@gmail.com', 'Fri Jun 14 2019 00:00:00 GMT+0530 (India Standard Time)', '1234556778', '', '', '44234424243', 'fsdfvvfdv', 'xasxasx', 'adcd', 1, '3,6', 1, 0, '1561383460');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subjects`
--

CREATE TABLE `tbl_subjects` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subjects`
--

INSERT INTO `tbl_subjects` (`id`, `title`, `status`) VALUES
(1, 'Math', 1),
(2, 'English', 0),
(3, 'CHEMISTRY', 1),
(4, 'PHYSICS', 0),
(5, 'BOTANY', 1),
(6, 'ZOOLOGY', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teachers`
--

CREATE TABLE `tbl_teachers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `subjects` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_teachers`
--

INSERT INTO `tbl_teachers` (`id`, `name`, `email`, `phone`, `profile_image`, `qualification`, `experience`, `subjects`, `class`, `status`, `created_on`) VALUES
(1, 'ankit', 'ankitkumar.nexus@gmail.com', '1234556778', '', 'm.c.a', '5 years', '1,3', '1,2', 1, '1561364711');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_time_table`
--

CREATE TABLE `tbl_time_table` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `time_table` longtext NOT NULL,
  `status` int(11) NOT NULL,
  `timestamp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_time_table`
--

INSERT INTO `tbl_time_table` (`id`, `title`, `time_table`, `status`, `timestamp`) VALUES
(6, 'Summer Time Table 2019', 'a:4:{i:1;a:7:{s:4:\"from\";s:7:\"7:00 PM\";s:2:\"to\";s:7:\"8:00 PM\";s:4:\"room\";s:1:\"1\";s:5:\"class\";s:1:\"1\";s:8:\"subjects\";s:1:\"1\";s:7:\"teacher\";s:1:\"1\";s:5:\"shift\";s:1:\"1\";}i:2;a:7:{s:4:\"from\";s:7:\"8:00 PM\";s:2:\"to\";s:7:\"9:00 PM\";s:4:\"room\";s:1:\"4\";s:5:\"class\";s:1:\"3\";s:8:\"subjects\";s:1:\"1\";s:7:\"teacher\";s:1:\"2\";s:5:\"shift\";s:1:\"2\";}i:3;a:7:{s:4:\"from\";s:7:\"7:00 PM\";s:2:\"to\";s:7:\"8:00 PM\";s:4:\"room\";s:1:\"2\";s:5:\"class\";s:1:\"2\";s:8:\"subjects\";s:1:\"2\";s:7:\"teacher\";s:1:\"2\";s:5:\"shift\";s:1:\"1\";}i:4;a:7:{s:4:\"from\";s:7:\"7:00 PM\";s:2:\"to\";s:7:\"8:00 PM\";s:4:\"room\";s:1:\"3\";s:5:\"class\";s:1:\"2\";s:8:\"subjects\";s:1:\"1\";s:7:\"teacher\";s:1:\"1\";s:5:\"shift\";s:1:\"2\";}}', 1, '1558441451');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_classes`
--
ALTER TABLE `tbl_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_time_table`
--
ALTER TABLE `tbl_time_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_classes`
--
ALTER TABLE `tbl_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_payments`
--
ALTER TABLE `tbl_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_time_table`
--
ALTER TABLE `tbl_time_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
