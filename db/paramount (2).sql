-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2019 at 07:59 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paramount`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `email`, `password`) VALUES
(1, 'Netgains', 'admin', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classes`
--

CREATE TABLE `tbl_classes` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_classes`
--

INSERT INTO `tbl_classes` (`id`, `title`, `value`) VALUES
(1, 'First class', 1),
(2, 'Second Class', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `type`, `value`) VALUES
(1, 'result_per_page', '100'),
(2, 'social_links', 'a:4:{s:8:\"facebook\";s:25:\"https://www.facebook.com/\";s:11:\"google_plus\";s:0:\"\";s:8:\"linkedin\";s:25:\"https://www.linkedin.com/\";s:7:\"twitter\";s:0:\"\";}'),
(3, 'contact_us', 'a:3:{s:5:\"phone\";s:0:\"\";s:5:\"email\";s:29:\"support@healthconnect.network\";s:7:\"address\";s:52:\"Unit 9, Level 1, 88 Station Rd Yeerongpilly QLD 4105\";}');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `father_phone` varchar(255) NOT NULL,
  `father_occupation` varchar(255) NOT NULL,
  `address` longtext NOT NULL,
  `school` varchar(255) NOT NULL,
  `class` int(11) NOT NULL DEFAULT '0',
  `subjects` longtext NOT NULL,
  `created_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`id`, `name`, `email`, `dob`, `phone`, `profile_image`, `father_phone`, `father_occupation`, `address`, `school`, `class`, `subjects`, `created_on`) VALUES
(1, 'Subhash Chand', 'subhash1120@gmail.com', '2019-05-23', '9914134456', '', '9914134456', '0 testisng ', '#89 badal colony zirakpur punjab', 'nEXUS tESCHIES', 0, 'ASDFADSF', '1558176453'),
(2, 'Subhash Chand', 'subhash1120@gmail.com.ar', '2019-05-15', '9914134456', 'img_1558177109_demo.png', '9882797895', '0 testisng ', '#89 badal colony zirakpur punjab', '', 0, '', '1558177109'),
(3, 'Testing', 'ankitkumar.nexus@gmail.com', '2019-05-02', '3165454', '', '216594613', 'abvghsfxfha', 'delhi', 'delhi', 0, 'English', '1558445586'),
(4, 'Testing', 'ankitkumar.nexus@gmail.com.ua', '2019-05-04', '3165454', '', '5', 'abvghsfxfha', 'delhi', 'delhi', 1, '1', '1558599346'),
(5, 'Testing', 'test@gmail.com.ua', '2019-05-10', '3165454', '', '124494', 'abvghsfxfha', 'delhi', 'delhi', 1, '1', '1558599494');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subjects`
--

CREATE TABLE `tbl_subjects` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subjects`
--

INSERT INTO `tbl_subjects` (`id`, `title`) VALUES
(1, 'Math'),
(2, 'English');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teachers`
--

CREATE TABLE `tbl_teachers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `subjects` varchar(255) NOT NULL,
  `created_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_teachers`
--

INSERT INTO `tbl_teachers` (`id`, `name`, `email`, `phone`, `profile_image`, `qualification`, `experience`, `subjects`, `created_on`) VALUES
(1, 'MR. RIAZ AHMAD GIRI', 'test@mail.com', '9914134456', 'img_1558178668_demo.png', 'M.Sc. /B.Ed', '13 Years', 'BOTANY,BOTANYr', '1558178668'),
(2, 'Testing', 'test.nexus@gmail.com', '3165454', '', '', '', 'Science', '1558445329');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_time_table`
--

CREATE TABLE `tbl_time_table` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `time_table` longtext NOT NULL,
  `timestamp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_time_table`
--

INSERT INTO `tbl_time_table` (`id`, `title`, `time_table`, `timestamp`) VALUES
(6, 'Summer', 'a:2:{i:1;a:7:{s:4:\"from\";s:7:\"7:00 PM\";s:2:\"to\";s:7:\"8:00 PM\";s:4:\"room\";s:1:\"1\";s:5:\"class\";s:1:\"0\";s:8:\"subjects\";s:1:\"1\";s:7:\"teacher\";s:1:\"1\";s:5:\"shift\";s:1:\"2\";}i:2;a:7:{s:4:\"from\";s:7:\"8:00 PM\";s:2:\"to\";s:7:\"9:00 PM\";s:4:\"room\";s:1:\"4\";s:5:\"class\";s:1:\"1\";s:8:\"subjects\";s:1:\"1\";s:7:\"teacher\";s:1:\"0\";s:5:\"shift\";s:1:\"1\";}}', '1558441451'),
(9, 'Winter new time table retse', 'a:2:{i:1;a:7:{s:4:\"from\";s:7:\"2:45 PM\";s:2:\"to\";s:7:\"2:45 PM\";s:4:\"room\";s:1:\"1\";s:5:\"class\";s:1:\"1\";s:8:\"subjects\";s:1:\"1\";s:7:\"teacher\";s:1:\"1\";s:5:\"shift\";s:1:\"1\";}i:2;a:7:{s:4:\"from\";s:7:\"3:45 PM\";s:2:\"to\";s:7:\"4:45 PM\";s:4:\"room\";s:1:\"0\";s:5:\"class\";s:1:\"2\";s:8:\"subjects\";s:1:\"2\";s:7:\"teacher\";s:1:\"0\";s:5:\"shift\";s:1:\"1\";}}', '1558512563');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_classes`
--
ALTER TABLE `tbl_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_time_table`
--
ALTER TABLE `tbl_time_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_classes`
--
ALTER TABLE `tbl_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_time_table`
--
ALTER TABLE `tbl_time_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
