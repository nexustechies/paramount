<?php 
/* 
	* Author: Subhash Shipu
	* Email: provider.nexus@gmail.com
	* Skype: provider.nexus@gmail.com

 */

class Common_model extends CI_Model 
	{
		public function __construct()
        {
            parent::__construct();
        }
		
		/* get meta tags */
		public function getMetaTags($page)		
		{
			$page = trim($page);
			if(empty($page)) { $page="index"; }
			$this->db->where('page',$page);
			$result = $this->db->get(SEO_TABLE)->result_array();
			if(!$result)
			{
				$this->db->where('page','index');
				$result = $this->db->get(SEO_TABLE)->result_array();
			}
			if(!$result)
			{
				$result = array(array('title' => 'Health Connect','keywords' => '','description'=>'','author'=>''));
			}
			return $result[0];
		}
		
		
		/* FUNCTION USED TO CHECK ROW EXISTS OR NOTE IN TABLE */
		public function checkExists($table,$where)
		{
			return $this->db->where($where)->from($table)->count_all_results();
		}
		/* Function used to get single row from table */
		public function GetSingleRow($table,$where)
		{
			$this->db->where($where);
			$query = $this->db->get($table);
			if($query)
			{
				return $query->row();
			}
			else
			{
				return false;
			}
		}
		/* Function used to get single value from table */
		public function GetSingleValue($table,$select,$where)
		{
			$this->db->select($select);
			$this->db->limit(1);
			$this->db->where($where);
			$query = $this->db->get($table);
			if($query)
			{
				return $query->row($select);
			}
			else
			{
				return false;
			}
		}
		/* Function used to get table rows  */
		public function GetTableRows($table,$where=null,$order='',$limit_perpage=0,$offset=0)
		{
			if(!empty($limit_perpage) )
			{
				$this->db->limit($limit_perpage, $offset);
			}
			if(!empty($where))
			{
				$this->db->where($where);
			}
			if(is_array($order))
			{
				$this->db->order_by($order[0],$order[1]);
			}
			$result = $this->db->get($table)->result_array();
			return $result;
		}

		public function GetSelectedFields($table,$fields,$where=null)
		{
			if(!empty($where))
			{
				$this->db->where($where);
			}
			if(!empty($fields))
			{
				$this->db->select($fields);
			}
			$result = $this->db->get($table)->result_array();
			return $result;
		}
		
		/* Funtion to get table fields name  */
		function get_table_fields($table)
		{
			$fields_name = [];
			$fields = $this->db->field_data($table);
			foreach($fields as $field)
			{
				$fields_name[] = $field->name;
			}
			unset($fields_name[0]);
			return $fields_name;
		}
	
		
		/* Function used to get user data from meta table; */
		public function get_user_meta($user_id,$meta_key="")
		{
			$condition['user_id'] = $user_id;
			if(!empty($meta_key))
			{
				$condition['meta_key'] = $meta_key;
			}
			$values = $this->common_model->GetTableRows(USERS_META_TABLE,$condition);
			$new_data = [];
			if($values)
			{
				foreach($values as $key => $value)
				{
					$new_data[$value['meta_key']] = $value['meta_value'];
				}
			}
			return $new_data;
		}
		
		/* Update meta data */
		public function update_user_meta_data($meta_data,$user_id)
		{
			foreach($meta_data as $data)
			{
				$meta_key = $data['meta_key'];
				$condition = array('meta_key' => $meta_key , 'user_id' => $user_id  );
				$key_exist = self::checkExists(USERS_META_TABLE,$condition);
				if($key_exist)
				{
					self::UpdateTableData(USERS_META_TABLE,$data,$condition);
				}
				else
				{
					self::InsertTableData(USERS_META_TABLE,$data);
				}
			}
		}
		
		/* Get coma saprated names by id */
		
		/* Update   Setting */
		public function update_settings($meta_data)
		{
			foreach($meta_data as $data)
			{ 
				$meta_key = $data['type'];
				$condition = array('type' => $meta_key);
				$key_exist = self::checkExists(SETTINGS_TABLE,$condition);
				if($key_exist)
				{
					self::UpdateTableData(SETTINGS_TABLE,$data,$condition);
				}
				else
				{
					self::InsertTableData(SETTINGS_TABLE,$data);
				}
			}
		}
		function get_coma_values($table,$ids)
		{
			$items = [];
			if(!empty($ids))
			{
				$explode = explode(',' ,$ids);
				foreach($explode as $item_id)
				{
					$title = $this->common_model->GetSingleValue($table,'title', array('id' => $item_id));
					array_push($items, $title);
				}
			}
			$titles = implode(', ',$items);
			return $titles;
		}
		
		
		/* FUNCTION USED TO SEND EMAIL ; */
		/* public function SendEmail($to,$from,$subject,$message) 
		{
			$black_list= array('::1','127.0.0.1'); // GRAB THE LOCALHOST IP ADDRESS
			
			if(!in_array($_SERVER['REMOTE_ADDR'],$black_list)) // CHECK IF NOT LOCALHOST
			{
				$this->email->from($from, 'Health Connect'); 
				$this->email->to($to);
				$this->email->subject($subject); 
				$this->email->message($message); 
				$this->email->set_mailtype("html");
				//Send mail 
				if($this->email->send()) // if successfully send 
				{
					return true ;
				}
				else
				{
					return false ;
				}
			}
		} */
		
		public function SendEmail($to,$from,$subject,$message) 
		{
			$black_list= array('::1','127.0.0.1'); // GRAB THE LOCALHOST IP ADDRESS
			$this->load->library("PhpMailerLib");
			$mail = $this->phpmailerlib->load();
			try {
				//Server settings
				/* $mail->SMTPDebug = 0;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP
				$mail->Host = 'ssl://smtp.googlemail.com';  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                                // Enable SMTP authentication
				$mail->Username = 'support@healthconnect.network';                 // SMTP username
				$mail->Password = 'Welcome1';                           // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 465; */


				// TCP port to connect to
				//Recipients
				$mail->setFrom($from, 'Paramount');
				$mail->addAddress($to);     // Add a recipient
				
				/* $mail->SMTPOptions = array(
					'ssl' => array(
						'verify_peer' => false,
						'verify_peer_name' => false,
						'allow_self_signed' => true
					)
				); */ 
			   //Content
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = $subject;
				$mail->Body    = $message;

				$mail->send();
				return true;
			} catch (Exception $e) {
				return false;
			}
			die();
			
		}
		
		public function CheckUserSession() // check user session
		{
			if(!$this->session->userdata('user_id') && !$this->session->userdata('role_id') )
			{
				redirect(base_url(), 'refresh' );
			}
		}
		
		public function CheckAdminSession() // check admin session
		{
			if(!$this->session->userdata('admin_id') )
			{
				redirect(base_url(), 'refresh' );
			}
		}
		
		public function GetTableResults($table,$where=null,$limit_perpage=0,$offset=0)
		{
			if(!empty($limit_perpage) )
			{
				$this->db->limit($limit_perpage, $offset);
			}
			if(!empty($where))
			{
				$this->db->where($where);
			}
			$result = $this->db->get($table)->result_array();
			return $result;
		}
		
		public function  GetJoinData($fields,$table,$join_table,$condition)
		{
			$this->db->select($fields);
			$this->db->from($table);
			$this->db->join($join_table.'s', $condition); 
			$query = $this->db->get();
			return $query->result_array();
		}
		
		/* insert data into table */
		public function InsertTableData($table,$data)
		{
			if($this->db->insert($table, $data))
			{
				$insert_id = $this->db->insert_id();
				return  $insert_id;
			}
			else 
			{
				return false ;
			}
		}
		
		/* insert multiple data into table */
		
		public function InsertMultipleData($table,$data)
		{
			if($this->db->insert_batch($table,$data))
			{
				$insert_id = $this->db->insert_id();
				return $insert_id;
			}
			else
			{
				return false;
			}
		}
		
		/* update table with new record */
		
		public function UpdateTableData($table,$data,$where)
		{
			$this->db->where($where);
			if($this->db->update($table, $data))
			{
				return true;
			}
			else
			{
				return false ;
			}
		}
		
		/* delete row  */
			
		public function DeleteTableData($table,$where)
		{
			$this->db->where($where);
			$this->db->delete($table);
			
			if( $this->db->affected_rows() )	
			{
				return true ;
			}
			else
			{
				return false ;
			}
		}
		
		public function GetTotalCount($table,$where="") // get total counts ;
		{
			if($where)
			{
				$this->db->where($where);
			}
			return $this->db->from($table)->count_all_results();
		}
		
		public function SelectDropdownIn($table,$option,$field,$value=array(),$where="",$extra_field = '',$order='') // selected
		{
			$html ="";
			if(!empty($where))
			{
				$this->db->where_in($where[0],$where[1]);
			}
			if(is_array($order))
			{
				$this->db->order_by($order[0],$order[1]);
			}
			$rows = $this->db->get($table)->result_array();
			foreach($rows as $row)
			{
				$selected = "";
				if(is_array($value))
				{
					if(in_array($row[$field],$value))
					{
						$selected = "selected";
					}
				}
				$efield = '';
				if(!empty($extra_field))
				{
					$efield = $row[$extra_field];
				}
				$html .= '<option value = "'.$row[$field].'" '.$selected.' extra_field = "'.$efield.'" >'.$row[$option].'</option>';
			}
			return $html;
		}
		
		public function get_job_locations_dropdown($parent_id=0,$value=[],$space="")
		{
			$count=0;
			$html = '';
			$space .= '&nbsp;&nbsp;&nbsp;';
			$special_chr="";
			if($parent_id <> 0)
			{
				$special_chr = '|-- ';
			}
			$this->db->where(array('parent_id' =>$parent_id ));
			$rows = $this->db->get(JOB_LOCATIONS_TABLE)->result_array();
			foreach($rows as $row)
			{
				$selected = "";
				if(is_array($value))
				{
					if(in_array($row['id'],$value))
					{
						$selected = "selected";
					}
				}
				$html .= '<option value = "'.$row['id'].'" '.$selected.' >'.$space.$special_chr.$row['title'].'</option>';
				$child_html = self::get_job_locations_dropdown($row['id'],$value,$space);
				$html .= $child_html;
			}
			return $html;
		}
		
		public function SelectDropdown($table,$option,$field,$value=array(),$where="",$extra_field = '',$order='') // selected
		{
			$html ="";
			if(!empty($where))
			{
				$this->db->where($where);
			}
			if(is_array($order))
			{
				$this->db->order_by($order[0],$order[1]);
			}
			$rows = $this->db->get($table)->result_array();
			foreach($rows as $row)
			{
				$selected = "";
				if(is_array($value))
				{
					if(in_array($row[$field],$value))
					{
						$selected = "selected";
					}
				}
				$efield = '';
				if(!empty($extra_field))
				{
					$efield = $row[$extra_field];
				}
				$html .= '<option value = "'.$row[$field].'" '.$selected.' extra_field = "'.$efield.'" >'.$row[$option].'</option>';
			}
			return $html;
		}
		
		function randomPassword() 
		{
			$alphabet = "abcdefghijklmnopqrstuwxyz0123456789";
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			return implode($pass); //turn the array into a string
		}
		
		public function GetAveRating($where)
		{
			
			$this->db->select('count(id) as rows');
			$this->db->select('sum(rating) as rating');
			$this->db->from('ratings');
			$this->db->where($where);
			$query = $this->db->get();
			if ( $query->num_rows() > 0 )
			{
				$row = $query->row_array();
				$json_data['rows'] = $row['rows'];
				$json_data['ratings'] = $row['rating'];
			}
			else
			{
				$json_data['rows'] = 0;
				$json_data['ratings'] = 0;
			}
			return json_encode($json_data);
		}
		
		public function GetReviews($where)
		{
			$this->db->select('*');
			$this->db->from('ratings');
			$this->db->where($where);
			$query = $this->db->get();
			$result = false;
			if ( $query->num_rows() > 0 )
			{
				$result = $query->result_array();
				
			}
			return $result;
		}
		
		public function GetUserAvatar($role_id=0,$user_id=0)
		{
			/*if($role_id ==0 && $user_id ==0)
			{
				$role_id =$this->session->userdata('role_id');
				$user_id=$this->session->userdata('user_id');
			}
		
			$table = $this->common_model->GetSingleValue('tbl_user_roles','user_table',array('id' =>$role_id ) );
			$avater = $this->common_model->GetSingleValue($table,'profile_pic',array('id' =>$user_id ) );
			if(!empty($avater) && is_file(dirname(APPPATH)."/uploads/avtars/".$avater))
			{
				$url = base_url("/uploads/avtars/".$avater);
			}
			else
			{*/
				$url = "https://png.icons8.com/ios/100/000000/guest-male.png";
			/*}*/

			return $url;
		}

	}