<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	private $controller_name;
	
	function __construct()
	{
		parent::__construct();
		$this->common_model->CheckAdminSession(); /// secure login 
		$this->controller_name = $this->router->fetch_class();
		$this->method_name = $this->router->fetch_method();
		$this->_module = $this->router->fetch_module();
		$this->path =$this->_module.'/'.$this->controller_name; 
		$this->load->helper('url');
		$this->load->library('user_agent');
	}
	
	public function index()
	{
		$where = [];
		$where = implode(' AND ', $where);
		
		$page_data['result_per_page'] = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'result_per_page'));
		$page_data['social_links'] = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'social_links'));
		$page_data['contact_us'] = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'contact_us'));
		$controllerName =  $this->_module.'/admin';
		 
		$page_data['module_path'] = $this->path;
		
		$module = $this->_module;
		$page_data['module'] = $module;
		
		$page_data['module_title'] = $module;
		$this->load->view('common/header');
		$this->load->view($module.'/detail',$page_data);
		$this->load->view('common/footer');
	}
	public function edit()
	{
		$module = $this->_module;
		$page_data['module_title'] = $module;
		$result_per_page = strip_tags($this->input->post('result_per_page'));
		$social_link = $this->input->post('social_link');
		$contact = $this->input->post('contact');
		$meta_data = array(
					array(
						'type' => 'result_per_page',
						'value' => $result_per_page,
					),
					array(
						'type' => 'social_links',
						'value' => serialize($social_link),
					),
					array(
						'type' => 'contact_us',
						'value' => serialize($contact),
					),
					
				);
			$update	= $this->common_model->update_settings($meta_data);
		
		if($update)
		{
			$this->session->set_flashdata('flash_message','Setting Successfully Updated.');
			$this->session->set_flashdata('class','success');
			redirect(base_url('settings/admin'));
			die(); 
		}
		else 
		{
			$this->session->set_flashdata('flash_message','Setting Successfully Updated.');
			$this->session->set_flashdata('class','success');
			redirect(base_url('settings/admin'));
			die();
		}
	}
	
}
?>
