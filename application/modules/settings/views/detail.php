<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE TITLE-->
			<div class="col-sm-12">
				<h1 class="page-title"> <?= ucfirst($module_title); ?> </h1>
			
			<div class="clearfix"></div>
			<?php if($this->session->flashdata('flash_message'))
				{ ?>
					<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" style="display: block;">
						<button class="close" data-close="alert"></button>
						<span> <?php echo $this->session->flashdata('flash_message'); ?> </span>
					</div>
					<?php
				}
			?>
			</div>
			<!-- END PAGE TITLE-->
			<!-- BEGIN SAMPLE TABLE PORTLET-->
			<div class="portlet box ">
				<div class="row">
					<div class="col-sm-12">
						<form class=" form-row-seperated" action="<?php echo base_url($module.'/admin/edit/') ?>" method="post" enctype="multipart/form-data">
							<div class="portlet">
								<div class="form-body">
									<div class="form-group">
										<?php  
										$phone = ''; 
										$email = '';
										$address = '';
										
										if(!empty($contact_us))
										{
											$contact = unserialize($contact_us);
											$phone = $contact['phone'];
											$address = $contact['address'];
											$email = $contact['email'];
											
											
										} 
										?> 
										<div class="col-sm-12">
											<h4 style="color: #62b5ec;"> Contact Us </h4>
										</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												<label>Phone Number:<span class="required">  </span></label>
												<input type="text" value="<?php echo $phone ?>" name="contact[phone]" class="form-control input-five"/>
											</div>
										</div>
										 
										<div class="col-sm-6">
											<div class="form-group">
												<label>E-Mail<span class="required"> </span></label>
												<input type="email" value="<?php echo $email ?>" name="contact[email]" class="form-control input-five"/>
											</div>
										</div>	
										
										<div class="col-sm-6">
											<div class="form-group">
												<label>Address:<span class="required"> </span></label>
												<input type="text" value="<?php echo $address ?>" name="contact[address]" class="form-control input-five"/>
											</div>
										</div>
										<?php  
										$facebook = ''; 
										$google_plus = '';
										$linkedin = '';
										$twitter = '';
										if(!empty($social_links))
										{
											$social_link = unserialize($social_links);
											$facebook = $social_link['facebook'];
											$google_plus = $social_link['google_plus'];
											$linkedin = $social_link['linkedin'];
											$twitter = $social_link['twitter'];
											
										} 
										?> 
										<div class="clearfix"></div>
										<div class="col-sm-12">
											<h4 style="color: #62b5ec;"> Social Links </h4>
										</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												<label>Facebook:<span class="required">  </span></label>
												<input type="url" value="<?php echo $facebook ?>" name="social_link[facebook]" class="form-control input-five"/>
											</div>
										</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												<label>Google+:<span class=""> </span></label>
												<input type="url" value="<?php echo $google_plus ?>" name="social_link[google_plus]" class="form-control input-five"/>
											</div>
										</div>	
										
										<div class="col-sm-6">
											<div class="form-group">
												<label>LinkedIn:<span class=""> </span></label>
												<input type="url" value="<?php echo $linkedin ?>" name="social_link[linkedin]" class="form-control input-five"/>
											</div>
										</div>	
										
										<div class="col-sm-6">
											<div class="form-group">
												<label>Twitter:<span class=""> </span></label>
												<input type="url" value="<?php echo $twitter ?>" name="social_link[twitter]" class="form-control input-five"/>
											</div>
										</div>
										<div class="col-sm-12">
											<h4 style="color: #62b5ec;"> Others </h4>
										</div>
										<div class="clearfix"></div>
										<div class="col-sm-6">
											<div class="form-group">
												<label>Results per page:<span class="required"> * </span></label>
												<input type="number" value="<?= $result_per_page; ?>" name="result_per_page" class="form-control input-five"/>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="actions btn-set text-right">
										<button type="button" onclick="window.location = '<?php echo base_url($this->path) ?>';"class="btn btn default">
											<i class="fa fa-angle-left"></i> Back
										</button>	
										<input type="hidden" name="s" value="ok">
										<button type="submit"  class="btn btn-success mt-ladda-btn ladda-button btn-outline" data-style="contract" data-spinner-color="#333">
											<i class="fa fa-check"></i> Save
										</button>
									</div>
									
								</div>
							</div>
						</form>
					</div>	
				</div>
			</div>
			<!-- END SAMPLE TABLE PORTLET-->
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->