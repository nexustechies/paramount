<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE TITLE-->
			<div class="col-md-12">
			<h1 class="page-title"> <?= $module_title; ?></h1>
			<?php 
				if($this->session->flashdata('flash_message'))
				{ ?>
					<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" style="display: block;">
						<button class="close" data-close="alert"></button>
						<span> <?php echo $this->session->flashdata('flash_message'); ?> </span>
					</div>
					<?php
				}
				if((validation_errors()))
				{
					?>
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?php echo validation_errors(); ?>
					</div>	
					<?php
				}
			?>
			</div>	
			<div class="row">
				<div class="col-md-12">
					<form class=" form-row-seperated" action="" method="post" enctype="multipart/form-data">
						<div class="portlet">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="" class="form-control-label">Title</label>
									<input type="text" class="form-control" name="title" value="<?=$title?>">
								</div>	
							</div>	
							<div class="col-sm-12">
								<div class="form-group">
									<label for="" class="form-control-label">Time Table</label>
									<div class="fac_container">
										<?php 
											$f_html = ''; 
											
											if(!empty($time_table))
											{
												$time = unserialize($time_table);
												
												foreach($time as $key => $time_table)
												{ 
													$shift_mor ='';
													$shift_eve ='';

													$action_btn = '';
													$classes = $this->common_model->SelectDropdown(CLASSES_TABLE,'title','id',array($time_table['class'])); ;
													$subjects = $this->common_model->SelectDropdown(SUBJECTS_TABLE,'title','id',array($time_table['subjects']),array('status' => 1)); ;
													$teacher = $this->common_model->SelectDropdown(TEACHERS_TABLE,'name','id',array($time_table['teacher']),array('status' => 1)); ;
													
													
													if($key > 1)
													{
														$action_btn = '<button type="button" class="btn btn-danger btn-sm remove_newf_row" title="Remove" >Remove</button>';
													}
													$room_one = check_selected($time_table['room'],1);
													$room_two = check_selected($time_table['room'],2);
													$room_three = check_selected($time_table['room'],3);
													$room_four = check_selected($time_table['room'],4);
													$shift_mor = check_selected($time_table['shift'],1);
													$shift_eve = check_selected($time_table['shift'],2);
													$f_html .= '<div class="single_time_container row" style="margin-top:10px;">
																	<div class="col-sm-11">
																		<div class="row">
																			<div class="col-sm-2 input-timerange" id="">
																				<label for="phone" class=" form-control-label">Time </label>
																				<input type="text" class="form-control from" value="'.$time_table['from'].'" name="time_table['.$key.'][from]">
																				<span class="input-group-addon"> to </span>
																				<input type="text" class="form-control to" value="'.$time_table['to'].'" name="time_table['.$key.'][to]">
																			</div>
																			<div class="col-sm-2">
																				<label for="phone"  class=" form-control-label">Room</label>
																				<select type="text"  name="time_table['.$key.'][room]"  class="form-control room">
																					<option value="0">Select Room</option>
																					<option '.$room_one .' value="1">Hall 1</option>
																					<option '.$room_two .' value="2">Hall 2</option>
																					<option '.$room_three .' value="3">Room 1</option>
																					<option '.$room_four .' value="4">Room 2</option>
																				
																				</select>
																			</div>
																			<div class="col-sm-2">
																				<label for="phone"  class=" form-control-label">Class</label>
																				<select type="text"  name="time_table['.$key.'][class]"  class="form-control class" >
																					<option value="0">Select class</option>
																					'.$classes.'
																				</select>
																			</div>
																			<div class="col-sm-2">
																				<label for="phone"  class=" form-control-label">Subject</label>
																				<select type="text"  name="time_table['.$key.'][subjects]"  class="form-control subjects">
																					<option value="0">Select Subject</option>
																					'.$subjects.'
																				</select>
																			</div>
																			<div class="col-sm-2">
																				<label for="phone"  class=" form-control-label">Teacher</label>
																				<select type="text"  name="time_table['.$key.'][teacher]"  class="form-control teacher">
																					<option value="0">Select Teacher</option>
																						'.$teacher.'
																				</select>
													 						</div>
																			<div class="col-sm-2">
																				<label for="phone"  class=" form-control-label">Shift</label>
																				<select type="text"  name="time_table['.$key.'][shift]"  class="form-control shift">
																					<option value="0">Select shift</option>
																					<option '.$shift_mor.' value="1">Morning shift</option>
																					<option '.$shift_eve.' value="2">Evening shift</option>
																				</select>
																			</div> 
																		</div>
																	</div>
																	<div class="col-sm-1 action_buttons " style="margin-top: 25px;">
																		'.$action_btn.'
																	</div>
																</div>';
												}
											} 
											else
											{
												$f_html .= '<div class="single_time_container row" style="margin-top:10px;">
																<div class="col-sm-11">
																	<div class="row">
																		<div class="col-sm-2 input-timerange" id="">
																			<label for="phone" class=" form-control-label">Time </label>
																			<input type="text" class="form-control from" name="time_table[1][from]">
																			<span class="input-group-addon"> to </span>
																			<input type="text" class="form-control to" name="time_table[1][to]">
																		</div>
																		<div class="col-sm-2">
																			<label for="phone"  class=" form-control-label">Room</label>
																			<select type="text"  name="time_table[1][room]"  class="form-control room">
																				<option value="0">Select Room</option>
																				<option value="1">Hall 1</option>
																				<option value="2">Hall 2</option>
																				<option value="3">Room 1</option>
																				<option value="4">Room 2</option>
																			
																			</select>
																		</div>
																		<div class="col-sm-2">
																			<label for="phone"  class=" form-control-label">Class</label>
																			<select type="text"  name="time_table[1][class]"  class="form-control class" >
																				<option value="0">Select class</option>
																				'.$classes.'
																			</select>
																		</div>
																		<div class="col-sm-2">
																			<label for="phone"  class=" form-control-label">Subjects</label>
																			<select type="text"  name="time_table[1][subjects]"  class="form-control subjects">
																				<option value="0">Select Subject</option>
																				'.$subjects.'
																			</select>
																		</div>
																		<div class="col-sm-2">
																			<label for="phone"  class=" form-control-label">Teacher</label>
																			<select type="text"  name="time_table[1][teacher]"  class="form-control teacher">
																				<option value="0">Select Teacher</option>
																				'.$teacher.'
																			</select>
																		</div>
																		<div class="col-sm-2">
																			<label for="phone"  class=" form-control-label">Shift</label>
																			<select type="text"  name="time_table[1][shift]"  class="form-control shift">
																				<option value="0">Select shift</option>
																				<option value="1">Morning shift</option>
																				<option value="2">Evening shift</option>
																				 
																			</select>
																		</div> 
																	</div>
																</div>
																<div class="col-sm-1 action_buttons hide" style="margin-top: 25px;">
																	
																</div>
															</div>';
											}
											echo $f_html;
										?>
									
									</div>	
								</div>
								<div class="form-group">
									<button type="button" class="btn btn-success btn-sm add_newf_row" style="margin-left: 15px;"> Add more  </button>
								</div>
							</div>
							
							<div class="clearfix"></div>
							<div class="col-md-12 actions btn-set text-right">
								<button type="button" onclick="window.location = '<?php echo base_url($this->path) ?>';"class="btn btn default">
									<i class="fa fa-angle-left"></i> Back
								</button>	
								<input type="hidden" name="s" value="ok">
								<button type="submit"  class="btn btn-success mt-ladda-btn ladda-button btn-outline" data-style="contract" data-spinner-color="#333">
									<i class="fa fa-check"></i> Save
								</button>
							</div>
						</div>
					</form>
				</div>	
			</div>	 
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<link href="<?php echo base_url(); ?>assets/admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/admin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/admin/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url(); ?>assets/admin/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/components-bootstrap-tagsinput.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/admin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
	<script>
	
	
	$('.input-timerange input').each(function() {
		$(this).timepicker();
	});
	
	$(document).on('click','.add_newf_row', function(e) 
	{
		var remove_btn_html = '<button type="button" class="btn btn-danger btn-sm remove_newf_row" > Remove </button>';
		var html = '<div class="single_time_container row" style="margin-top:10px;">';
		html += $('.single_time_container:first').html();
		html += '</div>';
		$('.fac_container').append(html);
		var total_container = $('.single_time_container').length;
		
		$('.single_time_container:last').find('.action_buttons').html(remove_btn_html);
		$('.single_time_container:last').find('.from').attr('name','time_table['+total_container+'][from]');
		$('.single_time_container:last').find('.to').attr('name','time_table['+total_container+'][to]');
		$('.single_time_container:last').find('.room').attr('name','time_table['+total_container+'][room]');
		$('.single_time_container:last').find('.class').attr('name','time_table['+total_container+'][class]');
		$('.single_time_container:last').find('.subjects').attr('name','time_table['+total_container+'][subjects]');
		$('.single_time_container:last').find('.teacher').attr('name','time_table['+total_container+'][teacher]');
		$('.single_time_container:last').find('.shift').attr('name','time_table['+total_container+'][shift]'); 
		
		$('.input-timerange input').each(function() {
			$(this).timepicker();
		});
	});
	
	$(document).on('click','.remove_newf_row', function(e) 
	{
		$(this).parents('.single_time_container:last').remove();
	});
	
	</script>
        
	<!-- END CONTENT -->
<style>
	.input-five
	{
		border-radius: 5px  !important;
	}
	span.select2-selection.select2-selection--single {
		border-radius: 5px !important;
	}
	li.select2-results__option {
    color: #000;
}

.close_image 
{
    display: inline-block;
    position: absolute;
    top: 0;
    right: 15px;
	cursor: pointer;
}
.profile_close_image 
{
    display: inline-block;
    position: absolute;
    top: 0;
    right: 15px;
	cursor: pointer;
}
	.hide
{
	display: none;
} 
.single_image .close_image {
        display: inline-block;
    position: absolute;
    top: 12PX;
    left: 263px;
    cursor: pointer;
}
</style>