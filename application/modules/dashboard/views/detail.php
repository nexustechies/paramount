	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<h1 class="page-title">Dashboard
				<small></small>
			</h1>
			 <div class="row">
				<?php 
				$role = $this->session->userdata('admin_role');
				if($role!=2):?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<a class="dashboard-stat dashboard-stat-v2 blue" href="<?php echo base_url('manage_teachers/admin')?>">
						<div class="visual">
							<i class="fa fa-comments"></i>
						</div>
						<div class="details">
							<div class="number">
								<span data-counter="counterup" data-value="<?=$total_teachers?>"><?=$total_teachers?></span>
							</div>
							<div class="desc"> Total Teachers </div>
						</div>
					</a>
				</div>
				<?php endif;?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<a class="dashboard-stat dashboard-stat-v2 red" href="<?php echo base_url('manage_students/admin')?>">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">
								<span data-counter="counterup" data-value="<?=$total_students?>"><?=$total_students?></span> </div>
							<div class="desc"> Total Students </div>
						</div>
					</a>
				</div>
				<?php 
				if($role!=2):?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<a class="dashboard-stat dashboard-stat-v2 green" href="<?php echo base_url('manage_subjects/admin') ?>">
						<div class="visual">
							<i class="fa fa-shopping-cart"></i>
						</div>
						<div class="details">
							<div class="number">
								<span data-counter="counterup" data-value="<?=$total_subjects?>"><?=$total_subjects?></span>
							</div>
							<div class="desc"> Total Subjects</div>
						</div>
					</a>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<a class="dashboard-stat dashboard-stat-v2 green" href="<?php echo base_url('manage_classes/admin')?>">
						<div class="visual">
							<i class="fa fa-shopping-cart"></i>
						</div>
						<div class="details">
							<div class="number">
								<span data-counter="counterup" data-value="<?=$total_classes?>"><?=$total_classes?></span>
							</div>
							<div class="desc"> Total Classes</div>
						</div>
					</a>
				</div>
				<?php endif;?>
			</div>
		</div>
	</div>