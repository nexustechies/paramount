<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	private $controller_name;
	
	function __construct()
	{
		parent::__construct();
		$this->common_model->CheckAdminSession(); /// secure login 
		$this->controller_name = $this->router->fetch_class();
		$this->method_name = $this->router->fetch_method();
		$this->_module = $this->router->fetch_module();
		$this->path =$this->_module.'/'.$this->controller_name;
		$this->admin_id = $this->session->userdata('admin_id');
	}
	
	public function logout()
	{
		$this->session->unset_userdata('admin_id');
		
		redirect(base_url('admin'), 'refresh' );
	}
	
	public function index()
	{
		$admin_id = $this->admin_id;
		$module = $this->_module; 
		
		$page_data['total_teachers'] = $this->common_model->GetTotalCount(TEACHERS_TABLE);
		$page_data['total_students'] = $this->common_model->GetTotalCount(STUDENTS_TABLE);
		$page_data['total_classes'] = $this->common_model->GetTotalCount(CLASSES_TABLE);
		$page_data['total_subjects'] = $this->common_model->GetTotalCount(SUBJECTS_TABLE);
		
		$page_data['module_title'] = ucfirst($module);
		$page_data['module'] = $module;
		$this->load->view('common/header',$page_data);
		$this->load->view($module.'/detail',$page_data);
		$this->load->view('common/footer');
	}
}
?>
