<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE TITLE-->
			<div class="col-md-12">
			<h1 class="page-title"> <?= $module_title; ?></h1>
			<?php 
				if($this->session->flashdata('flash_message'))
				{ ?>
					<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" style="display: block;">
						<button class="close" data-close="alert"></button>
						<span> <?php echo $this->session->flashdata('flash_message'); ?> </span>
					</div>
					<?php
				}
				if((validation_errors()))
				{
					?>
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?php echo validation_errors(); ?>
					</div>	
					<?php
				}
			?>
			</div>	
			<div class="row">
				<div class="col-md-12">
					<form class=" form-row-seperated" action="" method="post" enctype="multipart/form-data">
						<div class="portlet">
							<div class="form-body">
                                <div class="form-group"> 
									<div class="col-sm-3">
										<div class="form-group">
											<label>Name:</label>
											<input type="text" name="name" value="<?php echo set_value('name') ?>" class="form-control"/> 
											<?php echo form_error('name', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label>E-mail:</label>
											<input type="email" name="email" value="<?php echo set_value('email') ?>" class="form-control"/> 
											<?php echo form_error('email', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									
									<div class="col-sm-3">
										<div class="form-group">
											<label>Phone:</label>
											<input type="tel" name="phone" value="<?php echo set_value('phone') ?>" class="form-control"/> 
											<?php echo form_error('phone', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label>Qualification:</label>
											<input type="text" name="qualification" value="<?php echo set_value('qualification') ?>" class="form-control"/> 
											<?php echo form_error('qualification', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label>Experience:</label>
											<input type="text" name="experience" value="<?php echo set_value('experience') ?>" class="form-control"/> 
											<?php echo form_error('experience', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									
									<div class="col-sm-4">
										<div class="form-group">
											<label>Class:</label><br>
											<select class="form-control bs-select"  name="class[]" multiple>
												<option value="0"> --Select Subject--</option>
												<?php echo $class?>
											</select>
											
										</div>
									</div>
									
									<div class="col-sm-4">
										<div class="form-group">
											<label>Subjects:</label><br>
											<select class="form-control bs-select"   name="subjects[]" multiple>
												<option value="0"> --Select Subject--</option>
												<?php echo $subjects?>
											</select>
											
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Profile image:<span class="required">  </span></label>
											<input type="file" name="profile_image" value="<?php echo set_value('profile_image') ?>" class="form-control input-five" Placeholder="Profile Image"/>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-12 actions btn-set text-right">
									<button type="button" onclick="window.location = '<?php echo base_url($this->path) ?>';"class="btn btn default">
										<i class="fa fa-angle-left"></i> Back
									</button>	
									<input type="hidden" name="s" value="ok">
									<button type="submit"  class="btn btn-success mt-ladda-btn ladda-button btn-outline" data-style="contract" data-spinner-color="#333">
										<i class="fa fa-check"></i> Save
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>	
			</div>	
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<link href="<?php echo base_url(); ?>assets/admin/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/admin/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url(); ?>assets/admin/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        
	<!-- END CONTENT -->
<style>
	.input-five
	{
		border-radius: 5px  !important;
	}
	

</style>
<script>
	$(document).ready(function()
	{	
		($('.bs-select').length)
		{
			$('.bs-select').select2();
		}
	});
</script>