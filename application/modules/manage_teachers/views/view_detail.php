<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title"> <?= $module_title; ?> 
				<!--<span style="float: right;">
					<a href="<?php echo base_url($module_path.'/add') ?>" onClick = "showLoader();"  class="btn green">Add New</a>
				</span>-->
			</h1>
			<?php if($this->session->flashdata('flash_message'))
				{ ?>
					<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" style="display: block;">
						<button class="close" data-close="alert"></button>
						<span> <?php echo $this->session->flashdata('flash_message'); ?> </span>
					</div>
					<?php
				}
			?>
			<!-- END PAGE TITLE-->
			<div class="profile">
				<div class="tabbable-line tabbable-full-width">
					
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1_1">
							<div class="row">
								<div class="col-md-3">
									<ul class="list-unstyled profile-nav">
									<?php if($profile_image):?>
										<li>
											<img src="<?php echo base_url('uploads/'.$profile_image);?>" class="img-responsive pic-bordered" alt="">
										</li>
									<?php else:?>
										<li>
											<img src="https://cbie.ca/wp-content/uploads/2018/08/female-placeholder.jpg" class="img-responsive pic-bordered" alt="">
										</li>
									<?php endif; ?>
									</ul>
								</div>
								<div class="col-md-9">
									<div class="row">
										<div class="col-md-8 profile-info">
											<h1 class="font-green sbold uppercase"><?php echo ucfirst($name) ?></h1>
											
											<p>
												<a href="javascript:;">Email: <?php echo $email ?></a>
											</p>
											
											
											<p>
												Phone: <?php echo $phone ?>
											</p>
											
											
										</div>
										<!--end col-md-8-->
										<div class="col-md-4">
											<div class="portlet sale-summary">
												<div class="portlet-title">
													
												</div>
												<div class="portlet-body">
													
												</div>
											</div>
										</div>
										<!--end col-md-4-->
									</div>
									<!--end row-->
									<div class="tabbable-line tabbable-custom-profile">
										
										<div class="tab-content">
											<div class="tab-pane active" id="tab_1_11">
												<div class="portlet-body">
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>Qualification </th>
																<th>Experience </th>
																<th class="hidden-xs">Class </th>
																<th> Subject's </th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td><?php echo $qualification?> </td>
																<td><?php echo $experience?> </td>
																<td class="hidden-xs"> <?php echo $class ?> </td>
																<td class="hidden-xs"> <?php echo $subjects ?> </td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="col-md-12 actions btn-set text-right">
													<button type="button" onclick="window.location = '<?php echo base_url($this->path) ?>';"class="btn btn default">
														<i class="fa fa-angle-left"></i> Back
													</button>	
												</div>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->