<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	private $controller_name;
	
	function __construct()
	{
		parent::__construct();
		$this->common_model->CheckAdminSession(); /// secure login 
		$this->controller_name = $this->router->fetch_class();
		$this->method_name = $this->router->fetch_method();
		$this->_module = $this->router->fetch_module();
		$this->path =$this->_module.'/'.$this->controller_name; 
		$this->load->helper('url');
		$this->load->library('user_agent');
	}
	
	public function index()
	{
		$where = [];
		
		$where = implode(' and ',$where);
		
		$config["base_url"] = base_url().$this->path."/".$this->method_name;
		$config["total_rows"] = $this->common_model->GetTotalCount(TEACHERS_TABLE, $where);
		$result_per_page = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'result_per_page'));
		$config["per_page"] = $result_per_page;
		$config['uri_segment'] = 4;
		$limit = $config['per_page'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$page_data["results"] = $this->common_model->GetTableRows(TEACHERS_TABLE,$where,array('id','desc'),$limit,$page);
		$page_data["pagination"] = $this->pagination->create_links();
		$controllerName =  $this->_module.'/admin';
		$module = $this->_module;
		$page_data['module_title'] = ucfirst(str_replace('_',' ',$module));;
		$page_data['module_path'] = $this->path;
		$page_data['module'] = $module;
		$this->load->view('common/header');
		$this->load->view($module.'/detail',$page_data);
		$this->load->view('common/footer');
	}
	
	public function add()
	{
		$page = $this->input->post('page');
		if($this->input->post('s')) // if form submitted
		{
			$name = strip_tags($this->input->post('name'));
			$email = strip_tags($this->input->post('email'));
			$dob = strip_tags($this->input->post('dob'));
			$phone = strip_tags($this->input->post('phone'));
			$experience = strip_tags($this->input->post('experience'));
			$qualification = strip_tags($this->input->post('qualification'));
			$subjects = $this->input->post('subjects');
			$class = $this->input->post('class');
			if(!empty($subjects))
			{
				$subjects = implode(',', $subjects); 
			}
			if(!empty($class))
			{
				$class = implode(',', $class); 
			}
			
			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
			
			if ($this->form_validation->run() == true)
			{
				//$fields = $this->common_model->get_table_fields(TEACHERS_TABLE);
				$profile_image = self::upload_profile_image();
				
				$data = array
						(
							'name' =>  $name,
							'email' =>  $email,
							'phone' =>  $phone,
							'profile_image' =>  $profile_image,
							'experience' =>  $experience,
							'qualification' =>  $qualification,
							'subjects' =>  $subjects,
							'class' =>  $class,
							'status' => 1,
							'created_on' =>  time(),
						);
						
				$insert_id = $this->common_model->InsertTableData(TEACHERS_TABLE,$data);
				if($insert_id)
				{
					$this->session->set_flashdata('flash_message','Teacher Successfully Registered.');
					$this->session->set_flashdata('class','success');
					redirect(base_url($this->path));
					die();
				}
				else
				{
					$this->session->set_flashdata('flash_message','Something went wrong please try again.');
					$this->session->set_flashdata('class','danger');
					redirect(base_url($this->path));
					die();
				}
			}
		}
		$fileds = $this->common_model->get_table_fields(TEACHERS_TABLE);
		$page_data['module_path'] = $this->path;
		$module = $this->_module;
		$page_data['subjects'] =  $this->common_model->SelectDropdown(SUBJECTS_TABLE,'title','id',array($this->input->post('subjects')),array('status' => 1)); ;
		$page_data['class'] =  $this->common_model->SelectDropdown(CLASSES_TABLE,'title','id',array($this->input->post('class'))); ;
		$controllerName =  $this->_module.'/admin';
		$page_data['module_title'] = str_replace('_',' ',ucfirst($module))." : Add ";
		$this->load->view('common/header');
		$this->load->view($module.'/form/add',$page_data);
		$this->load->view('common/footer');
	}
	
	public function edit($id)
	{
		$page_data = (array)$this->common_model->GetSingleRow(TEACHERS_TABLE,array('id' => $id)); // get data
		if(!count($page_data)) { redirect(base_url($this->_module.'/admin')); }
		

		if($this->input->post('s')) // if form submitted
		{
			$name = strip_tags($this->input->post('name'));
			$email = strip_tags($this->input->post('email'));
			$dob = strip_tags($this->input->post('dob'));
			$phone = strip_tags($this->input->post('phone'));
			$experience = strip_tags($this->input->post('experience'));
			$father_phone = strip_tags($this->input->post('father_phone'));
			$father_occupation = strip_tags($this->input->post('father_occupation'));
			$address = strip_tags($this->input->post('address'));
			$school = strip_tags($this->input->post('school'));
			$subjects = $this->input->post('subjects');
			$class = $this->input->post('class'); 
			$qualification = strip_tags($this->input->post('qualification'));
			if(!empty($subjects))
			{
				$subjects = implode(',', $subjects); 
			}
		
		
			if(!empty($class))
			{
				$class = implode(',', $class); 
			}
			
			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
			
			if ($this->form_validation->run() == true)
			{
				//$fields = $this->common_model->get_table_fields(TEACHERS_TABLE);
				$profile_image = self::upload_profile_image();
				if(empty($profile_image))
				{
					$profile_image = $this->input->post('upload_profile_image');
				}
				
				$data = array
						(
							'name' =>  $name,
							'email' =>  $email,
							'phone' =>  $phone,
							'profile_image' =>  $profile_image,
							'experience' =>  $experience,
							'qualification' =>  $qualification,
							'subjects' =>  $subjects,
							'class' =>  $class,
						);
						
				$update = $this->common_model->UpdateTableData(TEACHERS_TABLE,$data,array('id' => $id));
				if($update)
				{
					$this->session->set_flashdata('flash_message','Teacher Successfully Updated.');
					$this->session->set_flashdata('class','success');
					redirect(base_url($this->path));
					die();
				}
				else
				{
					$this->session->set_flashdata('flash_message','Something went wrong please try again.');
					$this->session->set_flashdata('class','danger');
					redirect(base_url($this->path));
					die();
				}
			}
		}
		$page_data['module_path'] = $this->path;
		$module = $this->_module;
		$controllerName =  $this->_module.'/admin';
		$page_data['subjects'] =  $this->common_model->SelectDropdown(SUBJECTS_TABLE,'title','id',explode(',',$page_data['subjects']),array('status' => 1)); ;
		$page_data['class'] =  $this->common_model->SelectDropdown(CLASSES_TABLE,'title','id',explode(',',$page_data['class'])); ;
		$page_data['module_title'] = str_replace('_',' ',ucfirst($module))." : Edit ";
		$this->load->view('common/header');
		$this->load->view($module.'/form/edit',$page_data);
		$this->load->view('common/footer');
	}
	
	function upload_profile_image()
	{

		$profile_image = "";
		if(isset($_FILES['profile_image']['name']) && !empty($_FILES['profile_image']['name']))
		{
			$name = $_FILES['profile_image']['name'];
			$upload_dir = dirname(APPPATH)."/uploads";
			$t = explode(".", $name);
			$ext = end($t);

			$file_info = pathinfo($name);
			$file_name = $file_info['filename'];
			$replaced_name = preg_replace("/[^a-zA-Z]+/", "", $file_name);
			$name = $replaced_name.'.'.$file_info['extension'];
			
			$config['upload_path'] = dirname(APPPATH)."/uploads";
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['max_size'] = "20048";
			
			$config['file_name'] = 'img_'.time().'_'.$name;
			$this->load->library("upload", $config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('profile_image'))
			{
				$profile_image = $config['file_name'];
			}
		}
		return $profile_image;
	}
	public function change_status()
	{
		$value = $this->input->post('value');
		$id = $this->input->post('id');
		$update = $this->common_model->UpdateTableData(TEACHERS_TABLE,array('status'=>$value), array('id'=> $id));
		if($update)
		{
			
			$json_data['status'] = 'success';
			$json_data['html'] = 'Status successfully updated!';
		}
		else
		{
			$json_data['status'] = 'error';
			$json_data['html'] = 'Something went wrong, please try again later!!';
		}
		echo json_encode($json_data);
	}
	
	public function View_detail($id)
	{
		$page_data = (array)$this->common_model->GetSingleRow(TEACHERS_TABLE,array('id' => $id)); // get data
		if(!count($page_data)) { redirect(base_url($this->_module.'/admin')); }
		
		$page_data['module_path'] = $this->path;
		$module = $this->_module;
		$controllerName =  $this->_module.'/admin';
		$page_data['module_title'] = str_replace('_',' ','User Profile');
		$this->load->view('common/header');
		$this->load->view($module.'/view_detail',$page_data);
		$this->load->view('common/footer');
	}
}
?>
