<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE TITLE-->
			<div class="col-md-12">
			<h1 class="page-title"> <?= $module_title; ?></h1>
			<?php 
				if($this->session->flashdata('flash_message'))
				{ ?>
					<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" style="display: block;">
						<button class="close" data-close="alert"></button>
						<span> <?php echo $this->session->flashdata('flash_message'); ?> </span>
					</div>
					<?php
				}
				if((validation_errors()))
				{
					?>
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?php echo validation_errors(); ?>
					</div>	
					<?php
				}
			?>
			</div>	
			<div class="row">
				<div class="col-md-12">
					<form class=" form-row-seperated" action="" method="post" enctype="multipart/form-data">
						<div class="portlet">
							<div class="form-body">
                                <div class="form-group"> 
									<div class="col-sm-6">
										<div class="form-group">
											<label>Title:</label>
											<input type="text" name="title" value="<?php echo $title ?>" class="form-control"/> 
											<?php echo form_error('title', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Class:</label>
											<select class="form-control bs-select" name="class[]" multiple>
												<option>Select Class</option>
												<?php echo $class?>
											</select>
											<?php echo form_error('title', '<div class="error">', '</div>'); ?>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-12 actions btn-set text-right">
									<button type="button" onclick="window.location = '<?php echo base_url($this->path) ?>';"class="btn btn default">
										<i class="fa fa-angle-left"></i> Back
									</button>	
									<input type="hidden" name="s" value="ok">
									<button type="submit"  class="btn btn-success mt-ladda-btn ladda-button btn-outline" data-style="contract" data-spinner-color="#333">
										<i class="fa fa-check"></i> Save
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>	
			</div>	
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<link href="<?php echo base_url(); ?>assets/admin/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/admin/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url(); ?>assets/admin/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
	<script>
	$(document).on('click','.close_image',function(e)
	{
		e.preventDefault();
		$('.single_image').addClass('hide');
	});
	$(document).ready(function() {
		$('.bs-select').select2();
	}); 
	</script>
        
	<!-- END CONTENT -->
<style>
	.input-five
	{
		border-radius: 5px  !important;
	}
	span.select2-selection.select2-selection--single {
		border-radius: 5px !important;
	}
	li.select2-results__option {
    color: #000;
}

.close_image 
{
    display: inline-block;
    position: absolute;
    top: 0;
    right: 15px;
	cursor: pointer;
}
.profile_close_image 
{
    display: inline-block;
    position: absolute;
    top: 0;
    right: 15px;
	cursor: pointer;
}
	.hide
{
	display: none;
} 
.single_image .close_image {
        display: inline-block;
    position: absolute;
    top: 12PX;
    left: 263px;
    cursor: pointer;
}
</style>