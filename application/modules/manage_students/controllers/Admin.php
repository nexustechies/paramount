<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	private $controller_name;
	
	function __construct()
	{
		parent::__construct();
		$this->common_model->CheckAdminSession(); /// secure login 
		$this->controller_name = $this->router->fetch_class();
		$this->method_name = $this->router->fetch_method();
		$this->_module = $this->router->fetch_module();
		$this->path =$this->_module.'/'.$this->controller_name; 
		$this->load->helper('url');
		$this->load->library('user_agent');
	}
	
	public function index()
	{
		$where = [];
		$where = self::filter_query($where);
		$where = implode(' and ',$where);
		
		$config["base_url"] = base_url().$this->path."/".$this->method_name;
		$config["total_rows"] = $this->common_model->GetTotalCount(STUDENTS_TABLE, $where);
		$result_per_page = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'result_per_page'));
		$config["per_page"] = $result_per_page;
		$config['uri_segment'] = 4;
		$limit = $config['per_page'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$page_data["results"] = $this->common_model->GetTableRows(STUDENTS_TABLE,$where,array('id','desc'),$limit,$page);
		$page_data["pagination"] = $this->pagination->create_links();
		$controllerName =  $this->_module.'/admin';
		$module = $this->_module;
		$page_data['module_title'] = ucfirst(str_replace('_',' ',$module));;
		$page_data['module_path'] = $this->path;
		$page_data['module'] = $module;
		$this->load->view('common/header');
		$this->load->view($module.'/detail',$page_data);
		$this->load->view('common/footer');
	}
	public function filter_query($where = [])
	{
	
		foreach($_GET as $key => $value)
		{
			$value = strip_tags(($value));
			if($key <> "from" && $key <> "to")
			{
				if($key == "first_name" || $key == "last_name")
				{
					if($value <> "")
					{
						$where[] = "  $key LIKE '%$value%'   ";
					}
				}
				 
				else
				{
					if($value <> "")
					{
						$where[] = "  $key = '$value'   ";
					}
				}
			}
		}
		return $where; 
	}
	public function add()
	{
		$page = $this->input->post('page');
		if($this->input->post('s')) // if form submitted
		{
			$name = strip_tags($this->input->post('name'));
			$email = strip_tags($this->input->post('email'));
			$dob = strip_tags($this->input->post('dob'));
			$phone = strip_tags($this->input->post('phone'));
			$father_phone = strip_tags($this->input->post('father_phone'));
			$father_name = strip_tags($this->input->post('father_name'));
			$father_occupation = strip_tags($this->input->post('father_occupation'));
			$address = strip_tags($this->input->post('address'));
			$school = strip_tags($this->input->post('school'));
			$class = strip_tags($this->input->post('class'));
			$subjects = $this->input->post('subjects');
			
			//$teachers = $this->input->post('teachers');
			/* if(!empty($subjects))
			{
				$subjects = implode(',', $subjects); 
			}
			if(!empty($teachers))
			{
				$teachers = implode(',', $teachers); 
			} */
			
			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
			
			if ($this->form_validation->run() == true)
			{
				//$fields = $this->common_model->get_table_fields(STUDENTS_TABLE);
				$profile_image = self::upload_profile_image();
				
				$data = array
						(
							'name' =>  $name,
							'email' =>  $email,
							'dob' =>  $dob,
							'phone' =>  $phone,
							'profile_image' =>  $profile_image,
							'father_phone' =>  $father_phone,
							'father_name' =>  $father_name,
							'father_occupation' =>  $father_occupation,
							'address' =>  $address,
							'school' =>  $school,
							'class' =>  $class,
							'subjects' =>  serialize($subjects),
							//'teachers' =>  $teachers,
							'status' =>1,
							'created_on' =>  time(),
						);
						
				$insert_id = $this->common_model->InsertTableData(STUDENTS_TABLE,$data);
				if($insert_id)
				{
					$this->session->set_flashdata('flash_message','Student Successfully Registered.');
					$this->session->set_flashdata('class','success');
					redirect(base_url($this->path));
					die();
				}
				else
				{
					$this->session->set_flashdata('flash_message','Something went wrong please try again.');
					$this->session->set_flashdata('class','danger');
					redirect(base_url($this->path));
					die();
				}
			}
		}
		$fileds = $this->common_model->get_table_fields(STUDENTS_TABLE);
		$page_data['module_path'] = $this->path;
		$page_data['subjects'] =  $this->common_model->SelectDropdown(SUBJECTS_TABLE,'title','id',array($this->input->post('subjects')),array('status' => 1)); ;
		$page_data['class'] =  $this->common_model->SelectDropdown(CLASSES_TABLE,'title','id',array($this->input->post('class'))); ;
		$module = $this->_module;
		$controllerName =  $this->_module.'/admin';
		$page_data['module_title'] = str_replace('_',' ',ucfirst($module))." : Add ";
		$this->load->view('common/header');
		$this->load->view($module.'/form/add',$page_data);
		$this->load->view('common/footer');
	}
	
	public function edit($id)
	{
		$page_data = (array)$this->common_model->GetSingleRow(STUDENTS_TABLE,array('id' => $id)); // get data
		if(!count($page_data)) { redirect(base_url($this->_module.'/admin')); }
		

		if($this->input->post('s')) // if form submitted
		{
			$name = strip_tags($this->input->post('name'));
			$email = strip_tags($this->input->post('email'));
			$dob = strip_tags($this->input->post('dob'));
			$phone = strip_tags($this->input->post('phone'));
			$father_phone = strip_tags($this->input->post('father_phone'));
			$father_name = strip_tags($this->input->post('father_name'));
			$father_occupation = strip_tags($this->input->post('father_occupation'));
			$address = strip_tags($this->input->post('address'));
			$school = strip_tags($this->input->post('school'));
			$subjects = $this->input->post('subjects');
			//$teachers = $this->input->post('teachers');
			$class = $this->input->post('class');
			/* if(!empty($subjects))
			{
				$subjects = implode(',', $subjects); 
			}
			if(!empty($teachers))
			{
				$teachers = implode(',', $teachers); 
			} */
			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
			
			if ($this->form_validation->run() == true)
			{
				//$fields = $this->common_model->get_table_fields(STUDENTS_TABLE);
				$profile_image = self::upload_profile_image();
				if(empty($profile_image))
				{
					$profile_image = $this->input->post('upload_profile_image');
				}
				
				$data = array
						(
							'name' =>  $name,
							'email' =>  $email,
							'dob' =>  $dob,
							'phone' =>  $phone,
							'profile_image' =>  $profile_image,
							'father_phone' =>  $father_phone,
							'father_name' =>  $father_name,
							'father_occupation' =>  $father_occupation,
							'address' =>  $address,
							'school' =>  $school,
							'subjects' =>  serialize($subjects),
							//'teachers' =>  $teachers,
							'class' =>  $class,
						);
						
				$update = $this->common_model->UpdateTableData(STUDENTS_TABLE,$data,array('id' => $id));
				if($update)
				{
					$this->session->set_flashdata('flash_message','Student Successfully Updated.');
					$this->session->set_flashdata('class','success');
					redirect(base_url($this->path));
					die();
				}
				else
				{
					$this->session->set_flashdata('flash_message','Something went wrong please try again.');
					$this->session->set_flashdata('class','danger');
					redirect(base_url($this->path));
					die();
				}
			}
		}
		$page_data['module_path'] = $this->path;
		$module = $this->_module;
		$controllerName =  $this->_module.'/admin';
		//$page_data['subjects'] =  $this->common_model->SelectDropdown(SUBJECTS_TABLE,'title','id',explode(',',$page_data['subjects']),array('status' => 1)); ;
		$page_data['class'] =  $this->common_model->SelectDropdown(CLASSES_TABLE,'title','id',explode(',',$page_data['class'])); ;
		//$page_data['teachers'] =  $this->common_model->SelectDropdown(TEACHERS_TABLE,'name','id',explode(',',$page_data['teachers'])); ;
		$page_data['module_title'] = str_replace('_',' ',ucfirst($module))." : Edit ";
		$this->load->view('common/header');
		$this->load->view($module.'/form/edit',$page_data);
		$this->load->view('common/footer');
	}
	
	function upload_profile_image()
	{

		$profile_image = "";
		if(isset($_FILES['profile_image']['name']) && !empty($_FILES['profile_image']['name']))
		{
			$name = $_FILES['profile_image']['name'];
			$upload_dir = dirname(APPPATH)."/uploads";
			$t = explode(".", $name);
			$ext = end($t);

			$file_info = pathinfo($name);
			$file_name = $file_info['filename'];
			$replaced_name = preg_replace("/[^a-zA-Z]+/", "", $file_name);
			$name = $replaced_name.'.'.$file_info['extension'];
			
			$config['upload_path'] = dirname(APPPATH)."/uploads";
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['max_size'] = "20048";
			
			$config['file_name'] = 'img_'.time().'_'.$name;
			$this->load->library("upload", $config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('profile_image'))
			{
				$profile_image = $config['file_name'];
			}
		}
		return $profile_image;
	}
	// function used to perform action like enable/disable
	public function change_status()
	{
		$value = $this->input->post('value'); 
		$id = $this->input->post('id');
		$update = $this->common_model->UpdateTableData(STUDENTS_TABLE,array('status'=>$value), array('id'=> $id));
		if($update)
		{
			
			$json_data['status'] = 'success';
			$json_data['html'] = 'Status successfully updated!';
		}
		else
		{
			$json_data['status'] = 'error';
			$json_data['html'] = 'Something went wrong, please try again later!!';
		}
		echo json_encode($json_data);
	}
	public function payment_pending()
	{
		$value = $this->input->post('value'); 
		$id = $this->input->post('id');
		$update = $this->common_model->UpdateTableData(STUDENTS_TABLE,array('payment_pending'=>$value), array('id'=> $id));
		if($update)
		{
			
			$json_data['status'] = 'success';
			$json_data['html'] = 'Status successfully updated!';
		}
		else
		{
			$json_data['status'] = 'error';
			$json_data['html'] = 'Something went wrong, please try again later!!';
		}
		echo json_encode($json_data);
	}
	// payments module
	
	public function manage_payments($user_id)
	{
		$where = [];
		$where[] = " user_id = '$user_id' ";
		
		$where = implode(' and ',$where);
		
		$config["base_url"] = base_url().$this->path."/".$this->method_name;
		$config["total_rows"] = $this->common_model->GetTotalCount(PAYMENTS_TABLE, $where);
		$result_per_page = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'result_per_page'));
		$config["per_page"] = $result_per_page;
		$config['uri_segment'] = 4;
		$limit = $config['per_page'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$page_data["results"] = $this->common_model->GetTableRows(PAYMENTS_TABLE,$where,array('id','desc'),$limit,$page);
		$page_data["student"] = (array)$this->common_model->GetSingleRow(STUDENTS_TABLE,array('id'=>$user_id));
		$page_data["pagination"] = $this->pagination->create_links();
		$controllerName =  $this->_module.'/admin';
		$module = $this->_module;
		$page_data['module_title'] = ucfirst('Manage Payments');;
		$page_data['module_path'] = $this->path;
		$page_data['module'] = $module;
		$page_data['user_id'] = $user_id;
		
		$this->load->view('common/header');
		$this->load->view($module.'/payments',$page_data);
		$this->load->view('common/footer');
	}
	
	public function add_payments($user_id)
	{
		if($this->input->post('s')) // if form submitted
		{
			$paid_amount = strip_tags($this->input->post('paid_amount'));
			$pending_amount = strip_tags($this->input->post('pending_amount'));
			$total_amount = strip_tags($this->input->post('total_amount'));
			$paid_on = strip_tags($this->input->post('paid_on'));
			$description = strip_tags($this->input->post('description'));
			$subject = $this->input->post('subject');
			if(!empty($subject))
			{
				$subject = implode(',', $subject); 
			}
			
			$this->form_validation->set_rules('paid_amount', 'Paid amount', 'trim|required|xss_clean');
			
			if ($this->form_validation->run() == true)
			{
				$profile_image = self::upload_profile_image();
				
				$data = array
						(
							'user_id' =>  $user_id,
							'paid_amount' =>  $paid_amount,
							'pending_amount' =>  $pending_amount,
							'subject' =>  $subject,
							'total_amount' =>  $total_amount,
							'description' =>  $description,
							'paid_on' =>  $paid_on,
							'created_on' =>  time(),
						);
						
				$insert_id = $this->common_model->InsertTableData(PAYMENTS_TABLE,$data);
				if($insert_id)
				{
					$this->session->set_flashdata('flash_message','Payment Successfully added.');
					$this->session->set_flashdata('class','success');
					redirect(base_url($this->path.'/manage_payments/'.$user_id));
					die();
				}
				else
				{
					$this->session->set_flashdata('flash_message','Something went wrong please try again.');
					$this->session->set_flashdata('class','danger');
					redirect(base_url($this->path));
					die();
				}
			}
		}
		$sebject = $this->common_model->GetSingleValue(STUDENTS_TABLE,'subjects',array('id' => $user_id));
		if(!empty($sebject))
		{
			$s_sebject = [];
			$un_subjects = unserialize($sebject);
			foreach($un_subjects as $sub)
			{
				$sub_ids = $sub['subject'];
				array_push($s_sebject,$sub_ids);
			}
			$page_data['subjects'] = $s_sebject;
		}	
		$s_sebject = explode(',', $sebject);
		$page_data['module_path'] = $this->path;
		
		$module = $this->_module;
		$controllerName =  $this->_module.'/admin';
		$page_data['module_title'] = str_replace('_',' ','Manage Payments')." : Add ";
		$this->load->view('common/header');
		$this->load->view($module.'/form/add_payments',$page_data);
		$this->load->view('common/footer');
	}
	public function edit_payments($payment_id)
	{
		$page_data = (array)$this->common_model->GetSingleRow(PAYMENTS_TABLE,array('id' => $payment_id)); // get data
		if(!count($page_data)) { redirect(base_url($this->_module.'/admin')); }
		
		if($this->input->post('s')) // if form submitted
		{
			$paid_amount = strip_tags($this->input->post('paid_amount'));
			$pending_amount = strip_tags($this->input->post('pending_amount'));
			$total_amount = strip_tags($this->input->post('total_amount'));
			$paid_on = strip_tags($this->input->post('paid_on'));
			$description = strip_tags($this->input->post('description'));
			$subject = $this->input->post('subject');
			if(!empty($subject))
			{
				$subject = implode(',', $subject); 
			}
			
			$this->form_validation->set_rules('paid_amount', 'Paid amount', 'trim|required|xss_clean');
			
			if ($this->form_validation->run() == true)
			{
				$profile_image = self::upload_profile_image();
				
				$data = array
						(
							'paid_amount' =>  $paid_amount,
							'pending_amount' =>  $pending_amount,
							'subject' =>  $subject,
							'total_amount' =>  $total_amount,
							'description' =>  $description,
							'paid_on' =>  $paid_on,
							'created_on' =>  time(),
						);
						
				$insert_id = $this->common_model->UpdateTableData(PAYMENTS_TABLE,$data, array('id' => $payment_id));
				if($insert_id)
				{
					$this->session->set_flashdata('flash_message','Payment Successfully Updated.');
					$this->session->set_flashdata('class','success');
					redirect(base_url($this->path.'/manage_payments/'.$page_data['user_id']));
					die();
				}
				else
				{
					$this->session->set_flashdata('flash_message','Something went wrong please try again.');
					$this->session->set_flashdata('class','danger');
					redirect(base_url($this->path));
					die();
				}
			}
		}
		$sebject = $this->common_model->GetSingleValue(STUDENTS_TABLE,'subjects',array('id' => $page_data['user_id']));
		$s_sebject = explode(',', $sebject);
		
		
		$page_data['module_path'] = $this->path;
		$page_data['subjects'] = $s_sebject;
		$module = $this->_module;
		$controllerName =  $this->_module.'/admin';
		$page_data['module_title'] = str_replace('_',' ','Manage Payments')." : Edit ";
		$this->load->view('common/header');
		$this->load->view($module.'/form/edit_payments',$page_data);
		$this->load->view('common/footer');
	}
	public function View_detail($id)
	{
		
		$page_data = (array)$this->common_model->GetSingleRow(STUDENTS_TABLE,array('id' => $id)); // get data
		if(!count($page_data)) { redirect(base_url($this->_module.'/admin')); }

		$class = $this->common_model->GetSingleValue(CLASSES_TABLE,'title',array('id' => $page_data['class']));
		$subjects = $page_data['subjects'];
		$s_sebject = explode(',', $subjects);
		$subject_name = [];
		foreach($s_sebject as $s_seb)
		{
			$subject_n = $this->common_model->GetSingleValue(SUBJECTS_TABLE,'title',array('id' => $s_seb));
			array_push($subject_name,$subject_n);
		}
		$page_data['results'] = $this->common_model->GetTableRows(PAYMENTS_TABLE,array('user_id' => $id)); // get data
		$page_data['module_path'] = $this->path;
		$page_data['class'] = $class;
		$page_data['subject_name'] = implode(', ', $subject_name);
		$module = $this->_module;
		$controllerName =  $this->_module.'/admin';
		$page_data['module_title'] = str_replace('_',' ','User Profile');
		$this->load->view('common/header');
		$this->load->view($module.'/view_detail',$page_data);
		$this->load->view('common/footer');
	}
	public function get_subjects()
	{
		$value = $this->input->post('value');
		$class_ids = $this->common_model->GetTableRows(SUBJECTS_TABLE);
		$subject_names = [];
		foreach($class_ids as $class_id)
		{
			$class_array = explode(',', $class_id['class']);
			foreach($class_array as $class)
			{
				if($class == $value)
				{
					$subject = $class_id['id'];
					array_push($subject_names,$subject);
				}
			}
		}
		$html ='';
		foreach($subject_names as $subject_name)
		{
			$subject = $this->common_model->GetSingleValue(SUBJECTS_TABLE,'title',array('id' => $subject_name));
			$html .= '<option value="'.$subject_name.'">'.$subject.'</option>';
		}
		$data['html'] = $html;
		$data['status'] = "success";
		
		echo json_encode($data);
		die();
	}

	public function get_teachers()
	{
		$value = $this->input->post('value');
		$values  = explode(',',$value);
		$subjects_ids = $this->common_model->GetTableRows(TEACHERS_TABLE);
		$teachers_name = [];
		foreach($subjects_ids as $subjects_id)
		{
			$subjects_array =  $subjects_id['subjects'];
			if(in_array($subjects_array,$values))
			{
				$teacher = $subjects_id['id'];
				array_push($teachers_name,$teacher);
			}
		}

		$html ='';
		foreach($teachers_name as $teacher_name)
		{
			$teacher = $this->common_model->GetSingleValue(TEACHERS_TABLE,'name',array('id' => $teacher_name));
			$html .= '<option value="'.$teacher_name.'">'.$teacher.'</option>';
		}
		$data['html'] = $html;
		$data['status'] = "success";
		
		echo json_encode($data);
		die();
	}
	
}
?>
