<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE TITLE-->
			<div class="col-md-12">
			<h1 class="page-title"> <?= $module_title; ?></h1>
			<?php 
				if($this->session->flashdata('flash_message'))
				{ ?>
					<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" style="display: block;">
						<button class="close" data-close="alert"></button>
						<span> <?php echo $this->session->flashdata('flash_message'); ?> </span>
					</div>
					<?php
				}
				if((validation_errors()))
				{
					?>
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?php echo validation_errors(); ?>
					</div>	
					<?php
				}
			?>
			</div>	
			<div class="row">
				<div class="col-md-12">
					<form class=" form-row-seperated" action="" method="post" enctype="multipart/form-data">
						<div class="portlet">
							<div class="form-body">
                                <div class="form-group"> 
									<div class="col-sm-3">
										<div class="form-group">
											<label>Name:</label>
											<input type="text" name="name" value="<?php echo set_value('name') ?>" class="form-control"/> 
											<?php echo form_error('name', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label>E-mail:</label>
											<input type="email" name="email" value="<?php echo set_value('email') ?>" class="form-control"/> 
											<?php echo form_error('email', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label>Date Of Birth:</label>
											<input type="date" name="dob" value="<?php echo set_value('dob') ?>" class="form-control"/> 
											<?php echo form_error('dob', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label>Phone:</label>
											<input type="tel" name="phone" value="<?php echo set_value('phone') ?>" class="form-control"/> 
											<?php echo form_error('phone', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label>Father Name:</label>
											<input type="text" name="father_name" value="<?php echo set_value('father_name') ?>" class="form-control"/> 
											<?php echo form_error('father_name', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label>Father Phone:</label>
											<input type="tel" name="father_phone" value="<?php echo set_value('father_phone') ?>" class="form-control"/> 
											<?php echo form_error('father_phone', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label>Father Occupation:</label>
											<input type="text" name="father_occupation" value="<?php echo set_value('father_occupation') ?>" class="form-control"/> 
											<?php echo form_error('father_occupation', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label>Address:</label>
											<input type="text" name="address" value="<?php echo set_value('address') ?>" class="form-control"/> 
											<?php echo form_error('address', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label>School:</label>
											<input type="text" name="school" value="<?php echo set_value('school') ?>" class="form-control"/> 
											<?php echo form_error('school', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label>Class:</label>
											<select name="class" class="form-control " >
												<option value="0"> --Select Class--</option>
													<?php echo $class?>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12"> 
											<div class="stu_container">
												<div class="single_container row" style="margin-left: 1px;">
													<div class="col-sm-5">
														<div class="form-group">
															<label>Subjects:</label><br>
															<select name="subjects[1][subject]" class="form-control subjects" >
																<option value="0"> --Select Subject--</option>
																	
															</select>
															<?php echo form_error('subjects', '<div class="error">', '</div>'); ?>
														</div>
													</div>
													<div class="col-sm-5">
														<div class="form-group">
															<label>Teacher:</label><br>
															<select name="subjects[1][teacher]" class="form-control teachers" >
																<option value="0"> --Select Subject--</option>
																	
															</select>
															<?php echo form_error('subjects', '<div class="error">', '</div>'); ?>
														</div>
													</div>
													<div class="col-sm-2 action_buttons" style="margin-top: 25px;">
														
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-success btn-sm add_new_row" style="margin-left: 15px;"> Add more  </button>
									</div>
									<div class="clearfix"></div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Profile image:<span class="required">  </span></label>
											<input type="file" name="profile_image" value="<?php echo set_value('profile_image') ?>" class="form-control input-five" Placeholder="Profile Image"/>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-12 actions btn-set text-right">
									<button type="button" onclick="window.location = '<?php echo base_url($this->path) ?>';"class="btn btn default">
										<i class="fa fa-angle-left"></i> Back
									</button>	
									<input type="hidden" name="s" value="ok">
									<button type="submit"  class="btn btn-success mt-ladda-btn ladda-button btn-outline" data-style="contract" data-spinner-color="#333">
										<i class="fa fa-check"></i> Save
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>	
			</div>	
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<link href="<?php echo base_url(); ?>assets/admin/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/admin/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url(); ?>assets/admin/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
	
        
	<!-- END CONTENT -->
<style>
	.input-five
	{
		border-radius: 5px  !important;
	}
	span.select2-selection.select2-selection--single {
		border-radius: 5px !important;
	}
	li.select2-results__option {
    color: #000;
}
</style>
<script>
	$(document).ready(function() {
		$('.bs-select').select2();
	});
	
	$(document).on('change','select[name="class"]',function() {
		var value = $(this).val();
		var data = "value="+value; 
		var path = "manage_students/admin/get_subjects"
		var result = CallAjax(path,data);
		var status = result.status;
		if(status == "success")
		{

			$(document).find('select.subjects').html(result.html);
			
		}
	});
	
	$(document).on('change','select.subjects',function() {
		var value = $(this).val();
		var data = "value="+value; 
		var path = "manage_students/admin/get_teachers"
		var result = CallAjax(path,data);
		var status = result.status;
		if(status == "success")
		{
			$(this).parents('.single_container').find('select.teachers').html(result.html);
		}
	});

	$(document).on('click','.add_new_row', function(e) 
	{
		var remove_btn_html = '<button type="button" class="btn btn-danger btn-sm remove_newf_row" > Remove </button>';
		var html = '<div class="single_container row" style="margin-left: 1px;">';
		html += $('.single_container:first').html();
		html += '</div>';
		$('.stu_container').append(html);
		var total_container = $('.single_container').length;
		console.log(total_container);
		$('.single_container:last').find('.action_buttons').html(remove_btn_html);
		$('.single_container:last').find('.subjects').attr('name','subjects['+total_container+'][subject]');
		$('.single_container:last').find('.teachers').attr('name','subjects['+total_container+'][teacher]');

	});
	
	$(document).on('click','.remove_newf_row', function(e) 
	{
		$(this).parents('.single_container').remove();
	});

</script>
