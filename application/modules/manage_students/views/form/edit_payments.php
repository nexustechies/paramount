<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE TITLE-->
			<div class="col-md-12">
			<h1 class="page-title"> <?= $module_title; ?></h1>
			<?php 
				if($this->session->flashdata('flash_message'))
				{ ?>
					<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" style="display: block;">
						<button class="close" data-close="alert"></button>
						<span> <?php echo $this->session->flashdata('flash_message'); ?> </span>
					</div>
					<?php
				}
				if((validation_errors()))
				{
					?>
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?php echo validation_errors(); ?>
					</div>	
					<?php
				}
			?>
			</div>	
			
			<div class="row">
				<div class="col-md-12">
					<form class=" form-row-seperated" action="" method="post" enctype="multipart/form-data">
						<div class="portlet">
							<div class="form-body">
                                <div class="form-group"> 
									<div class="col-sm-3">
										<div class="form-group">
											<label>Subjects:</label><br>
											<select name="subject[]" class="form-control bs-select" multiple>
												<option value="0"> --Select Subject--</option>
												<?php if(!empty($subjects)) :

														$exploded_ids = explode(',',$subject);
														foreach($subjects as $sub):
														$sub_name =$this->common_model->GetSingleValue(SUBJECTS_TABLE,'title',array('id' => $sub));?>

														<option <?php if(in_array($sub,$exploded_ids)){ echo"selected"; } ?> value="<?php echo $sub?>"><?php echo $sub_name?></option>
												<?php endforeach; 
													endif;?>	
											</select>
											<?php echo form_error('subjects', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<label>Paid amount:</label>
											<input type="number" name="paid_amount" value="<?php echo $paid_amount ?>" class="form-control"/> 
											<?php echo form_error('paid_amount', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<label>Pending amount:</label>
											<input type="number" name="pending_amount" value="<?php echo $pending_amount ?>" class="form-control"/> 
											<?php echo form_error('pending_amount', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<label>Total amount:</label>
											<input type="number" name="total_amount" value="<?php echo $total_amount ?>" class="form-control"/> 
											<?php echo form_error('total_amount', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label>Paid On:</label>
											<input type="date" name="paid_on" value="<?php echo $paid_on ?>" class="form-control"/> 
											<?php echo form_error('paid_on', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label>Note:</label>
											<input type="text" name="description" value="<?php echo $description ?>" class="form-control"/> 
											<?php echo form_error('description', '<div class="error">', '</div>'); ?>
										</div>
									</div>
									
								</div>
								<div class="clearfix"></div>
								<div class="col-md-12 actions btn-set text-right">
									<button type="button" onclick="window.location = '<?php echo base_url($this->path) ?>';"class="btn btn default">
										<i class="fa fa-angle-left"></i> Back
									</button>	
									<input type="hidden" name="s" value="ok">
									<button type="submit"  class="btn btn-success mt-ladda-btn ladda-button btn-outline" data-style="contract" data-spinner-color="#333">
										<i class="fa fa-check"></i> Save
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>	
			</div>	
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<link href="<?php echo base_url(); ?>assets/admin/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/admin/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url(); ?>assets/admin/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
	
        
	<!-- END CONTENT -->
<style>
	.input-five
	{
		border-radius: 5px  !important;
	}
	span.select2-selection.select2-selection--single {
		border-radius: 5px !important;
	}
	li.select2-results__option {
    color: #000;
}
</style>
<script>
	$(document).ready(function() {
		$('.bs-select').select2();
	});
</script>

