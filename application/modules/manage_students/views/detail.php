<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title"> <?= $module_title; ?> 
				<span style="float: right;">
					<a href="<?php echo base_url($module_path.'/add') ?>" onClick = "showLoader();"  class="btn green">Add New</a>
				</span>
			</h1>
			<?php
				$role = $this->session->userdata('admin_role');
				if($role!=2)
				{
					echo $this->load->view($module.'/filter');
				}
			 if($this->session->flashdata('flash_message'))
				{ ?>
					<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" style="display: block;">
						<button class="close" data-close="alert"></button>
						<span> <?php echo $this->session->flashdata('flash_message'); ?> </span>
					</div>
					<?php
				}
			?>
			<!-- END PAGE TITLE-->
			<!-- BEGIN SAMPLE TABLE PORTLET-->
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs"></i><?= $module_title; ?></div>
					
				</div>
				<div class="portlet-body">
					<div class="">
						<table class="table table-striped table-bordered table-hover table-checkable" id="<?php if(count($results) > 0) { echo "datatable"; }  ?>">
							<thead>
								<tr>
									<th>Registration ID</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<?php 
									
									if($role!=2):?>
									<th>Payment History</th>
									<th>Payment Status</th>
									<th>Active</th>
									<?php endif; ?>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								if(count($results) > 0)
								{	
									foreach($results as $result)
									{
										$status = $result['status'] == 1 ? "checked" : "";
										$payment_pending = $result['payment_pending'] == 1 ? "checked" : "";
										$id = $result['id'];
										if($role==1)
										{ 
										?>
										<tr class="">
											<td> <a href="<?php echo base_url('manage_students/admin/manage_payments/'.$id); ?>"> <?php echo check_isset('id',$result); ?> </a> </td>
											<td> <a href="<?php echo base_url('manage_students/admin/manage_payments/'.$id); ?>"> <?php echo check_isset('name',$result); ?> </a> </td>
											<td> <a href="<?php echo base_url('manage_students/admin/manage_payments/'.$id); ?>"> <?php echo check_isset('email',$result); ?></a>  </td>
											<td> <a href="<?php echo base_url('manage_students/admin/manage_payments/'.$id); ?>"> <?php echo check_isset('phone',$result); ?> </a> </td>
											<td> <a href="<?php echo base_url('manage_students/admin/manage_payments/'.$id); ?>"> Check Payment History </a>  </td>
											<td> 
												<input <?= $payment_pending; ?> value="<?php echo $id; ?>" type="checkbox" data-on-text="Paid" data-size="mini" data-off-text="Pending" class="make-switch change_status" module="<?php echo $module_path.'/payment_pending'; ?>" data-on-color="success" data-off-color="danger" id="">
											</td>
											<td> 
												<input <?= $status; ?> value="<?php echo $id; ?>" type="checkbox" data-on-text="Yes" data-size="mini" data-off-text="No" class="make-switch change_status" module="<?php echo $module_path.'/change_status'; ?>" data-on-color="success" data-off-color="danger" id="">
											</td>
											<td>
												<a href= "<?php echo base_url($module_path.'/edit/'.$result['id']); ?>" class="btn btn-circle btn-icon-only btn-default tooltips" title = "Edit" href="javascript:;"> <i class="icon-note"></i></a>
												<a href= "<?php echo base_url($module_path.'/view_detail/'.$result['id']); ?>" class="btn btn-circle btn-icon-only btn-default tooltips" title = "Veiw Detail" href="javascript:;"> <i class="icon-eye"></i></a>
											</td>
										</tr>
										<?php
										}
										if($role==2)
										{ ?>
										<tr class="">
											<td>  <?php echo check_isset('id',$result); ?>  </td>
											<td>  <?php echo check_isset('name',$result); ?>  </td>
											<td>  <?php echo check_isset('email',$result); ?>  </td>
											<td>  <?php echo check_isset('phone',$result); ?>  </td>
											<td>
												<a href= "<?php echo base_url($module_path.'/edit/'.$result['id']); ?>" class="btn btn-circle btn-icon-only btn-default tooltips" title = "Edit" href="javascript:;"> <i class="icon-note"></i></a>
											</td>
										</tr>
										<?php
										}
									}
								}
								else
								{
									?>
										<tr class="no-records-found"><td colspan="15">No matching records found</td></tr>
									<?php
								}
								?>
							</tbody>
						</table>
						<div class = "pagination green">
							<?php echo $pagination; ?>
						</div>
					</div>
				</div>
			</div>
			<!-- END SAMPLE TABLE PORTLET-->
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
	<script>
	$(document).ready(function()
	{
		$(document).on('click','.delete',function(e)
		{
			e.preventDefault();
			var url = $(this).attr('href');
			swal(
			{
				title: "Are you sure?",
				text: "You will not be able to recover this!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes, I am sure!',
				cancelButtonText: "No, cancel it!",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function(isConfirm){

			   if (isConfirm){
				 
					location.href= url;
				}
			});
		});
	});
	 
	</script>