<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title"> <?= $module_title; ?> 
				<span style="float: right;">
					<a href="<?php echo base_url($module_path.'/add_payments/'.$user_id) ?>" onClick = "showLoader();"  class="btn green">Add New</a>
				</span>
			</h1>
			<?php if($this->session->flashdata('flash_message'))
				{ ?>
					<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" style="display: block;">
						<button class="close" data-close="alert"></button>
						<span> <?php echo $this->session->flashdata('flash_message'); ?> </span>
					</div>
					<?php
				}
			?>
			<!-- END PAGE TITLE-->
			<!-- BEGIN SAMPLE TABLE PORTLET-->
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs"></i><?= $module_title; ?></div>
					
				</div>
				<div class="portlet-body">
					<div class="">
						<table class="table table-striped table-bordered table-hover table-checkable" id="<?php if(count($results) > 0) { echo "datatable"; }  ?>">
							<thead>
								<tr>
									<th>Name</th>
									<th>Subject</th>
									<th>Paid Amount</th>
									<th>Due Amount</th>
									<th>Paid On</th>
									<th>Note</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								if(count($results) > 0)
								{	
									foreach($results as $result)
									{
										
										$subject_n = $this->common_model->GetSingleValue(SUBJECTS_TABLE,'title',array('id' => $result['subject']));
										$id = $result['id'];
										$im_subject = explode(',',$result['subject']);
										$subject_name = [];
										foreach($im_subject as $subject)
										{
											$subject_n = $this->common_model->GetSingleValue(SUBJECTS_TABLE,'title',array('id' => $subject));
											array_push($subject_name,$subject_n);
										}
										?>
										<tr class="">
											<td>  <?php echo check_isset('name',$student); ?>  </td>
											<td>  <?php echo implode(', ', $subject_name); ?>  </td>
											<td>  <?php echo check_isset('paid_amount',$result); ?>  </td>
											<td>  <?php echo check_isset('pending_amount',$result); ?>  </td>
											<td>  <?php echo check_isset('paid_on',$result); ?>  </td>
											<td>  <?php echo check_isset('description',$result); ?>  </td>
											<td>  <a href= "<?php echo base_url($module_path.'/edit_payments/'.$result['id']); ?>" class="btn btn-circle btn-icon-only btn-default tooltips" title = "Edit" href="javascript:;"> <i class="icon-note"></i></a> </td>
											
										</tr>
										<?php
									}
								}
								else
								{
									?>
										<tr class="no-records-found"><td colspan="15">No matching records found</td></tr>
									<?php
								}
								?>
							</tbody>
						</table>
						<div class = "pagination green">
							<?php echo $pagination; ?>
						</div>
					</div>
				</div>
			</div>
			<!-- END SAMPLE TABLE PORTLET-->
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
	<script>
	$(document).ready(function()
	{
		$(document).on('click','.delete',function(e)
		{
			e.preventDefault();
			var url = $(this).attr('href');
			swal(
			{
				title: "Are you sure?",
				text: "You will not be able to recover this!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes, I am sure!',
				cancelButtonText: "No, cancel it!",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function(isConfirm){

			   if (isConfirm){
				 
					location.href= url;
				}
			});
		});
	});
	 
	</script>