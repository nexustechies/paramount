<form action="">
   <div class="filters m-b-45">
      <div class="row" >
		<div class="col-sm-3">
			<div class="form_fields">
				<div class="row">
					
					<div class="col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="id" class=" form-control-label">Payment Pending/Paid</label>							
							<select class="form-control" name="payment_pending"> 								
								<option>--Select--</option>								
								<option value="0">Pending</option>								
								<option value="1">Paid</option>							
							</select>
										
						</div>
					</div>
				</div>
			</div>
		</div> 
		
		<div class="col-sm-3" style="margin-top: 24px">
			<div class="row ">
				<div class="col-sm-6">
                  <div class="form-group">							
				  <input type="submit" value="Filter"  class="form-control btn btn-success">					
				  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">							
				  <input type="reset"  value="Reset" url="<?php echo base_url($_SERVER['REDIRECT_QUERY_STRING']); ?>" class="form-control btn btn-danger">					
				  </div>
               </div>
            </div>
		</div>
		
      </div>
   </div>
</form>
<script>

$(document).on('click','[type="reset"]',function(e){	
	
	var url = $(this).attr('url');		
	location.href= url;
})
</script>

