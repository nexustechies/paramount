<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	private $controller_name;
	
	function __construct()
	{
		parent::__construct();
		$this->common_model->CheckAdminSession(); /// secure login 
		$this->controller_name = $this->router->fetch_class();
		$this->method_name = $this->router->fetch_method();
		$this->_module = $this->router->fetch_module();
		$this->path =$this->_module.'/'.$this->controller_name; 
		$this->load->helper('url');
		$this->load->library('user_agent');
	}
	
	public function index()
	{
		$where = [];
		
		$where = implode(' and ',$where);
		
		$config["base_url"] = base_url().$this->path."/".$this->method_name;
		$config["total_rows"] = $this->common_model->GetTotalCount(CLASSES_TABLE, $where);
		$result_per_page = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'result_per_page'));
		$config["per_page"] = $result_per_page;
		$config['uri_segment'] = 4;
		$limit = $config['per_page'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$page_data["results"] = $this->common_model->GetTableRows(CLASSES_TABLE,$where,array('id','desc'),$limit,$page);
		$page_data["pagination"] = $this->pagination->create_links();
		$controllerName =  $this->_module.'/admin';
		$module = $this->_module;
		$page_data['module_title'] = ucfirst(str_replace('_',' ',$module));;
		$page_data['module_path'] = $this->path;
		$page_data['module'] = $module;
		$this->load->view('common/header');
		$this->load->view($module.'/detail',$page_data);
		$this->load->view('common/footer');
	}
	
	public function add()
	{
		$page = $this->input->post('page');
		if($this->input->post('s')) // if form submitted
		{
			$title = strip_tags($this->input->post('title'));
			$value = strip_tags($this->input->post('value'));
			$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
			if ($this->form_validation->run() == true)
			{
				$data = array
						(
							'title' =>  $title,
							'value' =>  $value,
						);
						
				$insert_id = $this->common_model->InsertTableData(CLASSES_TABLE,$data);
				if($insert_id)
				{
					$this->session->set_flashdata('flash_message','Class Successfully added.');
					$this->session->set_flashdata('class','success');
					redirect(base_url($this->path));
					die();
				}
				else
				{
					$this->session->set_flashdata('flash_message','Something went wrong please try again.');
					$this->session->set_flashdata('class','danger');
					redirect(base_url($this->path));
					die();
				}
			}
		}
		$fileds = $this->common_model->get_table_fields(CLASSES_TABLE);
		$page_data['module_path'] = $this->path;
		$module = $this->_module;
		$controllerName =  $this->_module.'/admin';
		$page_data['module_title'] = str_replace('_',' ',ucfirst($module))." : Add ";
		$this->load->view('common/header');
		$this->load->view($module.'/form/add',$page_data);
		$this->load->view('common/footer');
	}
	
	public function edit($id)
	{
		$page_data = (array)$this->common_model->GetSingleRow(CLASSES_TABLE,array('id' => $id)); // get data
		if(!count($page_data)) { redirect(base_url($this->_module.'/admin')); }
		

		if($this->input->post('s')) // if form submitted
		{
			$title = strip_tags($this->input->post('title'));
			$value = strip_tags($this->input->post('value'));
			$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
			if ($this->form_validation->run() == true)
			{
				$data = array
						(
							'title' =>  $title,
							'value' =>  $value,
						);
						
				$update = $this->common_model->UpdateTableData(CLASSES_TABLE,$data,array('id' => $id));
				if($update)
				{
					$this->session->set_flashdata('flash_message','Class Successfully Updated.');
					$this->session->set_flashdata('class','success');
					redirect(base_url($this->path));
					die();
				}
				else
				{
					$this->session->set_flashdata('flash_message','Something went wrong please try again.');
					$this->session->set_flashdata('class','danger');
					redirect(base_url($this->path));
					die();
				}
			}
		}
		$page_data['module_path'] = $this->path;
		$module = $this->_module;
		$controllerName =  $this->_module.'/admin';
		
		$page_data['module_title'] = str_replace('_',' ',ucfirst($module))." : Edit ";
		$this->load->view('common/header');
		$this->load->view($module.'/form/edit',$page_data);
		$this->load->view('common/footer');
	}
	
	
	
}
?>
