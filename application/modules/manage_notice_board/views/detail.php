<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE TITLE-->
			<div class="col-sm-12">
				<h1 class="page-title"> <?= str_replace('_', ' ', ucfirst($module_title)); ?> </h1>
			
			<div class="clearfix"></div>
			<?php if($this->session->flashdata('flash_message'))
				{ ?>
					<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" style="display: block;">
						<button class="close" data-close="alert"></button>
						<span> <?php echo $this->session->flashdata('flash_message'); ?> </span>
					</div>
					<?php
				}
			?>
			</div>
			<!-- END PAGE TITLE-->
			<!-- BEGIN SAMPLE TABLE PORTLET-->
			<div class="portlet box ">
				<div class="row">
					<div class="col-sm-12">
						<form class=" form-row-seperated" action="<?php echo base_url($module.'/admin/edit/') ?>" method="post" enctype="multipart/form-data">
							<div class="portlet">
								<div class="form-body">
									<div class="form-group">
										<div class="col-sm-12">
											<h4 style="color: #62b5ec;"> Notice Board Detail </h4>
										</div>
										<div class="clearfix"></div>
										<div class="col-sm-6">
											<div class="form-group">
												<label>PDF :<span class="required">  </span></label>
												<input type="file" name="pdf"  class="form-control input-five" Placeholder="Profile Image"/>
											</div>
										</div>
										<?php if(!empty($pdf)) :?>
										<div class="col-sm-6 single_image">
											<div class="form-group">
												<label>PDF Link:<span class="required">  </span></label><br>
												<input type="hidden" name="uploade_pdf" value="<?=$pdf?>" class="form-control input-five" Placeholder="Profile Image"/>
												<a href="<?php echo base_url('pdf/'.$pdf);?>" style="height: 200px; width: 250px;">Download Admission Form</a>
												<span class="close_image"><i class="icon-close"></i></span>
											</div>
										</div>
									<?php endif;?>
										<div class="col-sm-12">
											<div class="form-group">
												<label>Coaching Institute :<span class="required"> * </span></label>
												<textarea type="text" id="summernote" name="coaching_institite" class="form-control input-five" rows="5"/><?php echo $coaching_institite ?></textarea>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<label>FAQ :<span class="required"> * </span></label>
												<textarea type="text" id="summernote_1" name="faq" class="form-control input-five" rows="5"/><?php echo $faq ?></textarea>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="actions btn-set text-right">
										<button type="button" onclick="window.location = '<?php echo base_url($this->path) ?>';"class="btn btn default">
											<i class="fa fa-angle-left"></i> Back
										</button>	
										
										<button type="submit"  class="btn btn-success mt-ladda-btn ladda-button btn-outline" data-style="contract" data-spinner-color="#333">
											<i class="fa fa-check"></i> Save
										</button>
									</div>
									
								</div>
							</div>
						</form>
					</div>	
				</div>
			</div>
			<!-- END SAMPLE TABLE PORTLET-->
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
	
	<link href="<?php echo base_url(); ?>assets/admin/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url(); ?>assets/admin/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
	
		$('#summernote').summernote();
		$('#summernote_1').summernote();
	});	

	$(document).on('click','.close_image',function(e)
	{
		e.preventDefault();
		$('.single_image').addClass('hide');
	});
	
</script>
<style>
.profile_close_image 
{
    display: inline-block;
    position: absolute;
    top: 0;
    right: 15px;
	cursor: pointer;
}
	.hide
{
	display: none;
} 
.single_image .close_image {
        display: inline-block;
    position: absolute;
    top: 12PX;
    left: 263px;
    cursor: pointer;
}
</style>
	