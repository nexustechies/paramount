<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
	private $controller_name;
	
	function __construct()
	{
		parent::__construct();
		$this->common_model->CheckAdminSession(); /// secure login 
		$this->controller_name = $this->router->fetch_class();
		$this->method_name = $this->router->fetch_method();
		$this->_module = $this->router->fetch_module();
		$this->path =$this->_module.'/'.$this->controller_name; 
		$this->load->helper('url');
		$this->load->library('user_agent');
	}
	
	public function index()
	{
		$where = [];
		$where = implode(' AND ', $where);
		
		
		$page_data['coaching_institite'] = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'coaching_institite'));
		$page_data['faq'] = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'faq'));
		$page_data['pdf'] = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'pdf'));

		$controllerName =  $this->_module.'/admin';
		 
		$page_data['module_path'] = $this->path;
		
		$module = $this->_module;
		$page_data['module'] = $module;
		
		$page_data['module_title'] = $module;
		$this->load->view('common/header');
		$this->load->view($module.'/detail',$page_data);
		$this->load->view('common/footer');
	}
	public function edit()
	{
		$module = $this->_module;
		$page_data['module_title'] = $module;
		$coaching_institite = $this->input->post('coaching_institite');
		$faq = $this->input->post('faq');
		$upload_pdf = self::upload_pdf();
		if(empty($upload_pdf))
		{
			$upload_pdf = $this->input->post('uploade_pdf');
		}
		$meta_data = array(
					
					array(
						'type' => 'coaching_institite',
						'value' => $coaching_institite,
					),
					array(
						'type' => 'faq',
						'value' => $faq,
					),
					array(
						'type' => 'pdf',
						'value' => $upload_pdf,
					),
					
				);
			$update	= $this->common_model->update_settings($meta_data);
		
		if($update)
		{
			$this->session->set_flashdata('flash_message','Notice Board Successfully Updated.');
			$this->session->set_flashdata('class','success');
			redirect(base_url('settings/admin'));
			die(); 
		}
		else 
		{
			$this->session->set_flashdata('flash_message','Notice Board  Successfully Updated.');
			$this->session->set_flashdata('class','success');
			redirect(base_url('manage_notice_board/admin'));
			die();
		}
	}
	function upload_pdf()
	{

		$pdf = "";
		if(isset($_FILES['pdf']['name']) && !empty($_FILES['pdf']['name']))
		{
			$name = $_FILES['pdf']['name'];
			$upload_dir = dirname(APPPATH)."/pdf";
			$t = explode(".", $name);
			$ext = end($t);

			$file_info = pathinfo($name);
			$file_name = $file_info['filename'];
			$replaced_name = preg_replace("/[^a-zA-Z]+/", "", $file_name);
			$name = $replaced_name.'.'.$file_info['extension'];
			
			$config['upload_path'] = dirname(APPPATH)."/pdf";
			$config['allowed_types'] = 'docx|pdf|rtf';
			$config['max_size'] = "20048";
			
			$config['file_name'] = 'img_'.time().'_'.$name;
			$this->load->library("upload", $config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('pdf'))
			{
				$pdf = $config['file_name'];
			}
		}
		return $pdf;
	}
	
}
?>
