<div class="info-color-dark py-4 z-depth-1">
    <div class="container">
        <div class="row">
            <div class="col-md-5 text-white">
                <div class="row">
                    <div class="col-md-4">
                        <span class="fa fa-bell float-lg-right" style="font-size: 32px"></span>
                    </div>
                    <div class="col-md-8">
                        <h4 class="text-uppercase  mb-0">Get Jobs Notification</h4>
                        <p>Free subscribe our newsletter now!!</p>
                    </div>
                </div>
			</div>
            <div class="col-md-4">
                <input type="" placeholder="Type Your Email Address" class="form-control mt-1 email_input" name="email">
            </div>
            <div class="col-md-3">
                <button class="btn btn-main m-0 subscribe_now">Subscribe</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).on('click','.subscribe_now',function(e){
	e.preventDefault();
	var email = $('.email_input').val();
	if(!isEmail(email))
	{
		var data = { type: "alert", status: "errot", html:"Please enter a valid email." };
		modalbox(data);
		return false;
	}
	
	var data = "email="+email;
	var result = CallAjax('ajax/subscribe',data);
	modalbox(result);
	
	
});

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
</script>