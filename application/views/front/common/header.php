<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" type="text/css" href="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/compiled-4.8.0.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.0/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/')?>css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sofia" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Lobster|Pacifico" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url('assets/front/')?>css/style.css">
    <title>Paramount Institute</title>
  </head>
  <body>
<div class="load">
    <div class="lds-roller" id="loader"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
   </div>
    <header class="fixed-top">
      <div class="top_header pt-1 pb-1   bg-dark">
        <div class="container">
			<div class="row">
				<div class="col-md-6 pt-3 fav">

					<i class="fab fa-facebook-f"></i>
					<i class="fab fa-twitter"></i>
					<i class="fab fa-google-plus-g"></i>
					<i class="fab fa-linkedin-in"></i>
				
				</div>
            <div class="col-md-6">
              <div class="text-md-right">
                
                  <a href="<?php echo base_url('front/register');?>" class="btn btn-danger"><i class="fa fa-user"></i> REGISTRATION</a>
                  <!--<a href="" class="btn btn-danger"><i class="fa fa-sign-in"></i> Login</a>-->
              </div>
        </div>
      </div>
        </div>
    </div>
      <nav class="navbar navbar-expand-lg navbar-dark bg-danger pb-3 pt-3">
        <div class="container">
          <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url('assets/front/')?>img/logo.png" width="50"> Paramount Institute</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item ">
                <a class="nav-link" href="<?php echo base_url();?>">HOME <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('front/about_us');?>">ABOUT US</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('front/time_table');?>">TIME TABLE</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('front/staff');?>">FACULTY</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">NOTICE BOARD</a>
              </li>
             
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('front/contact_us');?>">CONTACT US</a>
              </li>
            </ul>
          </div>
      </div>
      </nav>
    </header>



    <!-- slider --><br>
    <br>
    <br>
    <br>
    <br>
    <br>