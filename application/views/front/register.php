<div ng-controller="registerCtrl">
	<section class="register-login text-center text-white pt-5 pb-5" >
		<br>
		<br>
		<br>
		<h2><strong>Student Registration</strong></h2>
		<hr class="danger-color mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
	</section>


	<!-- main section -->
	<section class="register_form" >
		<div class="container">

			<div class="row">
				<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
					<form ng-submit="submitForm()"  method="post" class="needs-validation" novalidate enctype="multipart/form-data">
						<div class="student_img">
							<!--<img src="<?php echo base_url('assets/front/')?>img/avtar.png">-->
							<label>student's image</label>
							<input type="file" select-ng-files ng-model="fileArray" >
						</div>
						<div class="student_id">
							<h3>Student register </h3>
							<p>Please fill your detail</p>
						</div>
						<div class="student_detail">
							<div class="form-group">
								<label for="uname">Student Name:</label>
								<input type="text" class="form-control" ng-model="registerdata.name"  placeholder="Enter your name" name="name" required>
							</div>
							<div class="form-group">
								<label for="demo">Email:</label>
								<div class="input-group mb-3">
									<input type="text" class="form-control" ng-model="registerdata.email"  placeholder="Enter Email" name="email" required>
								</div>
							</div>
							<div class="form-group dob">
								<label for="uname">Date of Birth:</label>
								<input type="date" class="form-control" ng-model="registerdata.dob" placeholder="Date of birth" name="dob" required>
							</div>
							<div class="form-group number">
								<label for="uname">Phone Number:</label>
								<input type="number" class="form-control" ng-model="registerdata.phone" placeholder="Enter your Number" name="phone" required>
							</div>
							<div class="form-group pnumber">
								<label for="uname">Father's Number:</label>
								<input type="number" class="form-control" ng-model="registerdata.father_phone" placeholder="Father's number" name="father_phone" required>
							</div>
							<div class="form-group onumber">
								<label for="uname">Father's Occupation:</label>
								<input type="text" class="form-control" ng-model="registerdata.father_occupation" placeholder="Father's Occupation" name="father_occupation" required>
							</div>
							<div class="form-group">
								<label for="uname">Address:</label>
								<input type="text" class="form-control" ng-model="registerdata.address" placeholder="Address" name="address" required>
							</div>

							<div class="form-group">
								<label for="uname">School/College:</label>
								<input type="text" class="form-control" id="uname" ng-model="registerdata.school" placeholder="Name of your School/College" name="school" required>
							</div>
							<div class="form-group class_dropdown">
								<label for="uname">Select Your Class:</label>
								<select name="classes" class="custom-select" ng-model="registerdata.classes" required>									
									<?php echo $classes?>								
								</select>							
							</div>
							<div class="form-group subject_dropdown">								
								<label for="uname">Select Your Subject:</label>								
								<select name="subjects[]" class="custom-select bs-select" ng-model="registerdata.subjects" ng-multiple="true"  multiple>									
									<?php echo $subjects?>								
								</select>							
							</div>
							<p class="Declaration"><b>Declaration:</b> I, hereby declare that the entries made by me in the Application Form are complete and true to the best of my knowledge and based on records.</p>
							
							<div class="result_container alert alert-dismissible fade show d-none" role="alert">
								<div class="result_data"></div>
							</div>
							
							<div class="bottom-section">
								<div class="submit_button">
									<button type="submit" class="btn btn-default site_btn">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					<div class="side-img">


					</div>
				</div>
			</div>
		</div>
	</section>
</div><link href="<?php echo base_url(); ?>assets/admin/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />    <link href="<?php echo base_url(); ?>assets/admin/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />	<script src="<?php echo base_url(); ?>assets/admin/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>		<script>		$(document).ready(function() {		$('.bs-select').select2();	});	</script>