 <section class="time_table_banner main_banner text-center text-white pt-5 pb-5">
      <br>
      <br>
      <br>
      <h2><strong>Time Table</strong></h2>
      <hr class="danger-color mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
    </section>


    <!-- main section -->
	<section class="time_table">		
		<div class="container">		
			<div class="row">
				<div class="col-md-12">
					<h2>paramount institute of excellence kishtwar</h2>
					<hr>
					<h3>summer time table 2019</h3>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th scope="col">TIME</th>
									<th scope="col" colspan="2">HALL 1</th>
									<th scope="col" colspan="2">HALL 2</th>
									<th scope="col" colspan="2">ROOM NO 1</th>
									<th scope="col" colspan="2">ROOM NO 2</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<tr>
										<td rowspan="2" class="tym">06-07 AM</td>
										<td colspan="2"></td>
										<td colspan="2"></td>
										<td >MATH</td>
										<td >11TH</td>
										<td colspan="2"></td>
										
									</tr>
									<tr>
										<td colspan="2"></td>
										<td  colspan="2"></td>
										<td colspan="2">Mr. Zakir Hussain</td>
										<td  colspan="2"></td>
									</tr>
								 
								</tr>
								<tr>
									<tr>
										<td rowspan="2" class="tym">07-08 AM</td>
										<td >BOTANY</td>
										<td class="twelve">12th</td>
										<td >CHEMISTRY</td>
										<td >11TH</td>
										<td colspan="2"></td>
										<td >CHEMISTRY</td>
										<td >10TH</td>
										
										
									</tr>
									<tr>
										<td colspan="2">Mr. RIAZ AHMAD</td>
										<td  colspan="2">Mr. ALI IMRAN HUSSAIN</td>
										<td colspan="2"></td>
										<td  colspan="2">Mr. IMTIYAZ AHMAD</td>
									</tr>
								 
								</tr>
								<tr>
									<tr>
										<td rowspan="2" class="tym">08-09 AM</td>
										<td >CHEMISTRY</td>
										<td class="twelve">12th</td>
										<td >PHYSICS</td>
										<td >11TH</td>
										<td >BOTANY</td>
										<td >11TH</td>
										<td >MATH</td>
										<td >10TH</td>
										
										
									</tr>
									<tr>
										<td colspan="2">Mr. ALI IMRAN HUSSAIN</td>
										<td  colspan="2">Mr. MANZOOR AHMAD</td>
										<td colspan="2">Mr. RIAZ AHMAD</td>
										<td  colspan="2">Mr. Zakir Hussain</td>
									</tr>
								 
								</tr>
								<tr>
									<tr>
										<td rowspan="2"class="tym">09-10 AM</td>
										<td >PHYSICS</td>
										<td class="twelve">12th</td>
										<td colspan="2"></td>
										<td >MATH</td>
										<td >11TH</td>
										<td colspan="2"></td>
										
									</tr>
									<tr>
										<td colspan="2" >Mr. MANZOOR AHMAD</td>
										<td  colspan="2"></td>
										<td colspan="2">Mr. RAKESH KUMAR</td>
										<td  colspan="2"></td>
									</tr>
								 
								</tr>
								<tr>
									<td colspan="9"><h3> Evening shift</h3></td>
								 
								</tr>
								<tr>
									<tr>
										<td rowspan="2" class="tym">04-05 PM</td>
										<td >ZOOLOGY</td>
										<td >11TH</td>
										<td colspan="2"></td>
										<td >MATH</td>
										<td class="twelve">12th</td>
										<td >BIOLOGY</td>
										<td >10TH</td>
										
									</tr>
									<tr>
										<td colspan="2">MR. ADNAN RAJA</td>
										<td  colspan="2"></td>
										<td colspan="2">Mr. RAKESH KUMAR</td>
										<td  colspan="2">MR. NAVEED AHMAD</td>
									</tr>
								 
								</tr>
								<tr>
									<tr>
										<td rowspan="2" class="tym">05-06 PM</td>
										<td >BOTANY</td>
										<td >11TH</td>
										<td >ZOOLOGY</td>
										<td class="twelve">12th</td>
										<td >MATH</td>
										<td class="twelve">12th</td>
										<td >PHYSICS</td>
										<td >10TH</td>
										
									</tr>
									<tr>
										<td colspan="2">MR. RIAZ AHMAD</td>
										<td  colspan="2">MR. ADNAN RAJA</td>
										<td colspan="2">MR. ZAKIR HUSSAIN</td>
										<td  colspan="2">MISS NOWREEN</td>
									</tr>
								 
								</tr>
								<tr>
									<tr>
										<td rowspan="2" class="tym">06-07 PM</td>
										<td >BOTANY</td>
										<td class="twelve">12th</td>
										<td colspan="2"></td>
										<td >MATH</td>
										<td >11TH</td>
										<td colspan="2"></td>
										
									</tr>
									<tr>
										<td colspan="2">MR. RIAZ AHMAD</td>
										<td  colspan="2"></td>
										<td colspan="2">Mr. Zakir Hussain</td>
										<td  colspan="2"></td>
									</tr>
								 
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>