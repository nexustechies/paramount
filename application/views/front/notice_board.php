<div ng-controller="noticeBoardCtrl">
<section class="main_banner text-center text-white pt-5 pb-5">
  <br>
  <br>
  <br>
  <h2><strong>Notice Board</strong></h2>
  <hr class="danger-color mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
</section>


    <!-- main section -->
	<section class="notice-board">		
		<div class="container">		
			<div class="row">
				<div class="col-md-12">
					<h5>A COMPREHENSIVE COACHING INSTITUTE FOR:</h5>
					<div class="coach" ng-bind-html="coaching_institite"></div>
				</div>
				<div class="col-md-12">
					<div class="pdf-btn" ng-if="pdf">
						<h5><a href="{{pdf}}" download target="_blank" class="btn">Download Admission Form</a></h5>
					</div>
				</div>
				<div class="col-md-12">
					<div class="" ng-bind-html="faq"></div>
				</div>
			</div>
		</div>
	</section>
</div>