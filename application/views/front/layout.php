<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Paramount Institute</title>
	
	
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.5/css/mdb.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/')?>css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sofia" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Lobster|Pacifico" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/front/')?>css/style.css">
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   
    <script>
        var BASE_URL = "<?php echo site_url(); ?>/";
    </script>
	<style>
	#on_load_loader{
		width: 100%;
		background: url(https://www.creditmutuel.fr/cmne/fr/banques/webservices/nswr/images/loading.gif) no-repeat center center rgb(255, 255, 255);
		float: left;
		height: 433px;
		margin-top: 156px;

	}
	</style>
</head>

<body ng-app="myApp">
    <div class="load">
        <div class="lds-roller" id="loader">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <header class="fixed-top">
        <div class="top_header pt-1 pb-1   bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 pt-3 fav" ng-controller="sociallinkCtrl">	
						
						<a  ng-if="facebook != ''" href="{{facebook}}"  target="_blank">
						<i class="fab fa-facebook-f"></i></a>	
							
						<a ng-if="twitter != ''" href="{{twitter}}" target="_blank">
						<i class="fab fa-twitter"></i></a>
								
						<a ng-if="google_plus != ''" href="{{google_plus}}"  target="_blank">
						<i class="fab fa-google-plus-g"></i></a>	
						
						<a ng-if="linkedin != ''" href="{{linkedin}}"  target="_blank">
						<i class="fab fa-linkedin-in"></i></a>
						
                    </div>
                    <div class="col-md-6">
                        <div class="text-md-right">

                            <a href="/register" class="btn btn-danger"><i class="fa fa-user"></i> REGISTRATION</a>
                            <!--<a href="" class="btn btn-danger"><i class="fa fa-sign-in"></i> Login</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-danger pb-3 pt-3">
            <div class="container">
                <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url('assets/front/')?>img/logo.png" width="50"> Paramount Institute</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto" ng-controller="NavigationCtrl">
                        <li class="nav-item" ng-class="{ active: isCurrentPath('/') }">
                            <a class="nav-link" href="/">{{home}} <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item" ng-class="{ active: isCurrentPath('/about') }">
                            <a class="nav-link" href="/about">{{about}}</a>
                        </li>
                        <li class="nav-item" ng-class="{ active: isCurrentPath('/time-table') }">
                            <a class="nav-link" href="/time-table">{{time_table}}</a>
                        </li>
                        <li class="nav-item" ng-class="{ active: isCurrentPath('/staff') }">
                            <a class="nav-link" href="/staff">{{staff}}</a>
                        </li>
                        <li class="nav-item" ng-class="{ active: isCurrentPath('/notice-board') }">
                            <a class="nav-link" href="/notice-board">{{notice_board}}</a>
                        </li>

                        <li class="nav-item" ng-class="{ active: isCurrentPath('/contact-us') }">
                            <a class="nav-link" href="/contact-us">{{contact_us}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
	<section ng-if="stateIsLoading" ><div id="on_load_loader"></div></section>
	<!-- slider -->
	<br>
    <br>
    <br>
    <br>
    <br>
    <br>
	<div class="main">
        <div ng-view></div>
	</div>
	<footer class="page-footer font-small special-color-dark pt-4">

        <!-- Footer Elements -->
        <div class="container" ng-controller="sociallinkCtrl">

            <!-- Social buttons -->
            <ul class="list-unstyled list-inline text-center">		
				<li class="list-inline-item"  ng-if="facebook != ''"  >			
					<a href="{{facebook}}" class="btn-floating btn-fb mx-1" target="_blank">	
						<i class="fab fa-facebook-f"> </i>		
					</a>
                </li>			
				
                <li class="list-inline-item"  ng-if="twitter != ''"  >
                    <a href="{{twitter}}" class="btn-floating btn-tw mx-1" target="_blank">
                <i class="fab fa-twitter"> </i>
              </a>
                </li>				
				
                <li class="list-inline-item"  ng-if="google_plus != ''"  >
                    <a href="{{google_plus}}" class="btn-floating btn-gplus mx-1" target="_blank">
						<i class="fab fa-google-plus-g"> </i>
					</a>
                </li>				
                <li class="list-inline-item"  ng-if="linkedin != ''"  >
                    <a href="{{linkedin}}" class="btn-floating btn-li mx-1" target="_blank">
                <i class="fab fa-linkedin-in"> </i>
              </a>
                </li>				
			</ul>
            <!-- Social buttons -->
		</div>
        <!-- Footer Elements -->
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">
            <p>© 2019 Copyright Paramount Institute</p>
            <p>Website by
                <a href="https://www.nexustechies.com/" target="_blank">Nexus Techies</a></p>
        </div>
		<!-- Copyright -->

    </footer>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.5/js/mdb.min.js"></script>
	
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-sanitize.js"></script>
	
    <script type="text/javascript" src="<?php echo base_url('assets/front/')?>js/app.js"></script>
	
</body>

</html>