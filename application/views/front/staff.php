<div ng-controller="staffCtrl">	
	<section class="staff-banner register-login text-center text-white pt-5 pb-5">
      <br>
      <br>
      <br>
      <h2><strong>Staff</strong></h2>
      <hr class="danger-color mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
    </section>
	<!-- main section -->
	<section class="main_staff">
		<div class="container">
			<div class="row" ng-if="teachers.length > 0">
				<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12" ng-repeat="teacher in teachers" >
					<div class="staff_detail">
                        <img src="{{teacher.profile_image}}" alt="{{teacher.name}}">
						<h5>{{teacher.name}}</h5>
						<p ng-if="teacher.subjects != ''" ><b>Subject Teaching:</b> {{teacher.subjects}}</p>
						<p ng-if="teacher.qualification != ''"><b>Qualification:</b> {{teacher.qualification}}</p>
						<p ng-if="teacher.experience != ''"><b>Experience:</b> {{teacher.experience}}</p>
						<p ng-if="teacher.phone != ''"><b>Ph. No:</b> {{teacher.phone}}</p>
					</div>
				</div>
			</div>	
		</div>				
	</section>
</div>	