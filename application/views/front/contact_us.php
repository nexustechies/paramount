<div ng-controller="contactCtrl">
<section class="main_banner text-center text-white pt-5 pb-5">
      <br>
      <br>
      <br>
      <h2><strong>Contact Us</strong></h2>
	  <hr class="danger-color mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
    </section>
    <!-- Section: Contact v.1 -->
<section class="my-5">

<div class="container">
  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-md-5 mb-5">

      <!-- Form with header -->
      <div class="card">
        <div class="card-body">
          <!-- Header -->
          <div class="form-header bg-danger">
            <h3 class="mt-2"><i class="fas fa-envelope"></i> Write to us:</h3>
          </div>
          <p class="dark-grey-text">We'll write rarely, but only the best content.</p>
          <!-- Body -->
		<form ng-submit="submitForm()"  method="post" class="needs-validation" novalidate enctype="multipart/form-data">
		
          <div class="md-form">
            <i class="fas fa-user prefix grey-text"></i>
            <input type="text" ng-model="contactdata.name" id="form-name" name="name" class="form-control">
            <label for="form-name">Your name</label>
          </div>
          <div class="md-form">
            <i class="fas fa-envelope prefix grey-text"></i>
            <input type="text" id="form-email" ng-model="contactdata.email" name="email" class="form-control">
            <label for="form-email">Your email</label>
          </div>
          <div class="md-form">
            <i class="fas fa-tag prefix grey-text"></i>
            <input type="text" id="form-Subject" ng-model="contactdata.subject" name="subject" class="form-control">
            <label for="form-Subject">Subject</label>
          </div>
          <div class="md-form">
            <i class="fas fa-pencil-alt prefix grey-text"></i>
            <textarea id="form-text"  ng-model="contactdata.message" name="message" class="form-control md-textarea" rows="3"></textarea>
            <label for="form-text">Send message</label>
          </div>
		  <div class="result_container alert alert-dismissible fade show d-none" role="alert">
			<div class="result_data"></div>
		 </div>
          <div class="text-center">
            <button class="btn btn-danger">Submit</button>
          </div>
		  </form>
        </div>
		
      </div>
      <!-- Form with header -->
    </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-7">

          <!--Google map-->
          <div id="map-container-section" class="z-depth-1-half map-container-section mb-4" style="height: 400px;">
            <iframe src="https://maps.google.com/maps?q=Manhatan&t=&z=15&ie=UTF8&iwloc=&output=embed" width="100%" height="400" frameborder="0"
              style="border:0" allowfullscreen></iframe>
          </div>
          <!-- Buttons-->
          <div class="row text-center">
            <div class="col-md-4">
              <a class="btn-floating blue accent-1">
                <i class="fas fa-map-marker-alt"></i>
              </a>			 
              <p>{{address}}</p>
            </div>
            <div class="col-md-4">
              <a class="btn-floating blue accent-1">
                <i class="fas fa-phone"></i>
              </a> 
              <p>{{phone}}</p>
              
            </div>
            <div class="col-md-4">
              <a class="btn-floating blue accent-1">
                <i class="fas fa-envelope"></i>
              </a>
              <p>{{email}}</p>
             
            </div>
          </div>

        </div>
        <!-- Grid column -->
      </div>
      </div>
      <!-- Grid row -->

    </section>
</div>	