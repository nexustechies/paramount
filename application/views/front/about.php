<div ng-controller="aboutCtrl">	<section class="main_banner text-center text-white pt-5 pb-5">
      <br>
      <br>
      <br>
      <h2><strong>About Us</strong></h2>
      <hr class="danger-color mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
    </section>


    <!-- main section -->
    <div class="first_section pd-3">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="">
              <br>
              <br>
              <br>
              <h4 class="mb-3 text-danger text- second-font"><strong>About Paramount Institute</strong></h4>
              <hr class="danger-color mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
              <p class="text-justify">The Paramount Institute of Excellence is established with the aim to provide the highest quality education & Guidance to the students of this valley of Sapphire, Saffron & Shrines. This is the door to the world of your aspirations. Paramount Institute of Excellence approaches to inculcate the thrust and motivation in the young minds for excelling in this world of competition.</p>
              <p>Any educational institute can grow successfully only when innovation and teaching mesh together- one without the other is incomplete. Therefore we have ensured the development of a collaborative environment conducive to learning, exposure to the best learning practices and promotion of innovation and creativity.</p>
              <a href="/contact-us" class="btn btn-danger ml-0">Contact Us</a>
              <br>
              <br>
            </div>
          </div>
          <div class="col-md-6">
            <img src="https://learnsafe.com/wp-content/uploads/2017/12/IT-Guy-Isolated.png" class="img-fluid">
          </div>
        </div>
      </div>
    </div></div>	