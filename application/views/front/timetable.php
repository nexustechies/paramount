<div ng-controller="timetableCtrl">
<section class="time_table_banner main_banner text-center text-white pt-5 pb-5">
      <br>
      <br>
      <br>
      <h2><strong>Time Table</strong></h2>
      <hr class="danger-color mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
    </section>


    <!-- main section -->
	<section class="time_table">		
		<div class="container">		
			<div class="row">
				<div class="col-md-12">
					<h2>paramount institute of excellence kishtwar</h2>
					<hr>
					<h3>{{title}}</h3>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th scope="col">TIME</th>
									<th scope="col" colspan="2">HALL 1</th>
									<th scope="col" colspan="2">HALL 2</th>
									<th scope="col" colspan="2">ROOM NO 1</th>
									<th scope="col" colspan="2">ROOM NO 2</th>
								</tr>
							</thead>
							<tbody ng-bind-html="timetabledata"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>	