<div ng-controller="indexCtrl">
    <div class="slider">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url('assets/front/')?>img/hero_slider_4.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="<?php echo base_url('assets/front/')?>img/hero_slider_5.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="<?php echo base_url('assets/front/')?>img/slider-6.jpg" class="d-block w-100" alt="...">
                </div>
            </div>

        </div>
        <div class="overlay_slider text-white" style="    position: absolute;z-index: 99;width: 100%;top: 230px;">
            <div class="container d-none d-xs-block d-lg-block">
                <div class="text-center"><br>
                    <img src="<?php echo base_url('assets/front/') ;?>img/logo.png" width="150">
                    <h1 class="main-font  weight800">Welcome To <span style="color: red">Paramount</span> Institute</h1>
                </div>
            </div>
		</div>
    </div>


    <!-- main section -->
    <div class="first_section pd-3">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="">
                        <br>
                        <br>
                        <br>
                        <h4 class="mb-3 text-danger text- second-font"><strong>Paramount Institute</strong></h4>
                        <hr class="danger-color mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                        <p class="text-justify">The Paramount Institute of Excellence is established with the aim to provide the highest quality education & Guidance to the students of this valley of Sapphire, Saffron & Shrines. This is the door to the world of your aspirations.
                            Paramount Institute of Excellence approaches to inculcate the thrust and motivation in the young minds for excelling in this world of competition.</p>
                        <p>Any educational institute can grow successfully only when innovation and teaching mesh together- one without the other is incomplete. Therefore we have ensured the development of a collaborative environment conducive to learning,
                            exposure to the best learning practices and promotion of innovation and creativity.</p>
                        <a href="/about" class="btn btn-danger ml-0">Read More</a>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="https://learnsafe.com/wp-content/uploads/2017/12/IT-Guy-Isolated.png" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
    <!--staff section-->
    <section class="staff_sec">
        <div class="container">
            <div class="main_title">
                <h2>Meet Our Faculty</h2>
                <hr>
                <p>There is a moment in the life of any aspiring astronomer that it is time to buy that first telescope. It’s exciting to think about setting up your own viewing station.</p>
            </div>
            <div class="row team_inner" ng-if="teachers.length > 0">
				<div class="col-lg-3 col-sm-6" ng-repeat="teacher in teachers  | limitTo:4" >
					 <div class="team_item">
                        <div class="team_img">
							<img class="rounded-circle" src="{{teacher.profile_image}}" alt="">
						</div>
						<div class="team_name">	
							 <h4>{{teacher.name}} </h4>
						</div>
						<p>{{teacher.subjects}}</p>
					</div>
				</div>
			</div>
            <div class="veiw_more_btn">
                <a href="/staff" class=" btn btn-danger ml-0">view more</a>
            </div>
        </div>
    </section>

    <!--staff section-->
    <!--MISSION section-->
    <section class="mission_section pd-3">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="<?php echo base_url('assets/front/')?>img/3.png" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <div class="mission-text">

                        <h4 class="mb-3 text-danger text- second-font"><strong>OUR MISSION</strong></h4>
                        <hr class="danger-color mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                        <p class="text-justify">Lorem ipsum nullam tortor consequat amet felis dapibus.Lorem ipsum nullam tortor consequat amet felis dapibus.Lorem ipsum nullam tortor consequat amet felis dapibus.Lorem ipsum nullam tortor consequat amet felis dapibus.Lorem ipsum
                            nullam tortor consequat amet felis dapibus.Lorem ipsum nullam tortor consequat amet felis dapibus.Lorem ipsum nullam tortor consequat amet felis dapibus.</p>
                        <ul>
                            <li>Lorem ipsum nullam tortor consequat amet felis dapibus.</li>
                            <li>Lorem ipsum nullam tortor consequat amet felis dapibus.</li>
                            <li>Lorem ipsum nullam tortor consequat amet felis dapibus.</li>
                            <li>Lorem ipsum nullam tortor consequat amet felis dapibus.</li>
                            <li>Lorem ipsum nullam tortor consequat amet felis dapibus.</li>
                        </ul>
                        <a href="/about" class="btn btn-danger ml-0">Read More</a>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--staff section-->


    <div class="parralexx pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8 text-center text-white  mt-5 mb-5">
                    <h5>The Paramount Institute of Excellence is established with the aim to provide the highest quality education & Guidance to the students of this valley of Sapphire, Saffron & Shrines. This is the door to the world of your aspirations.</h5>
                    <br>
                    <button class="btn btn-outline-light">
					<a href="/contact-us" class="text-white">Get In Touch</a>
				</button>
                </div>
            </div>
        </div>
    </div>
</div>