<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
</div>

            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> Paramount - <?php echo date('Y') ?> &copy; Copyright
               </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
			
			<div class="modal fade " id="MyModal" tabindex="-1" role="basic" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Start Dragging Here</h4>
						</div>
						<div class="modal-body"> Modal body goes here </div>
						<div class="modal-footer">
							<button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
							<span></span>
						</div>
						<p class ="ajax_loader" style = "text-align: center;display: none;"><img src = "<?php echo base_url('assets/admin/gif') ?>/ajax-loader.gif" ></p>
					</div>
					<!-- /.modal-content -->
					
				</div>
				<!-- /.modal-dialog -->
				
				
			</div>
			<!-- END FOOTER -->
        </div>
        <!--[if lt IE 9]>
<script src="<?php echo base_url('assets/admin/'); ?>global/plugins/respond.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>global/plugins/excanvas.min.js"></script> 
<script src="<?php echo base_url('assets/admin/'); ?>global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
       <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('assets/admin/'); ?>global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url('assets/admin/'); ?>pages/scripts/dashboard.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url('assets/admin/'); ?>layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
		 <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/admin/'); ?>pages/scripts/ui-sweetalert.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
		<script src="<?php echo base_url('assets/admin/'); ?>global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>


		<!-- Button spinner -->
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/ladda/spin.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/ladda/ladda.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
       
		<script src="<?php echo base_url('assets/admin/'); ?>global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
		<!-- Datatable -->
		
		<script src="<?php echo base_url('assets/admin/'); ?>global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/admin/'); ?>js/custom.js" type="text/javascript"></script>
		


	</body>
</html>