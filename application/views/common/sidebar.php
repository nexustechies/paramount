<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- BEGIN SIDEBAR -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
				<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<li class="sidebar-toggler-wrapper hide">
					<div class="sidebar-toggler">
						<span></span>
					</div>
				</li>
				<!-- END SIDEBAR TOGGLER BUTTON -->
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="nav-item start <?php if($this->router->fetch_module() == "dashboard") { echo "active"; } ?>">
					<a href="<?php echo base_url('dashboard/admin'); ?>" class="nav-link nav-toggle" onClick = "showLoader();">
						<i class="icon-home"></i>
						<span class="title">Dashboard</span>
						<span class="selected"></span>
					</a>
				</li>
				
				<li class="nav-item start <?php if($this->router->fetch_module() == "manage_students") { echo "active"; } ?>">
					<a href="<?php echo base_url('manage_students/admin'); ?>" class="nav-link nav-toggle" onClick = "showLoader();">
						<i class="icon-users"></i>
						<span class="title">Manage Students</span>
						<span class="selected"></span>
					</a> 
				</li> 
				<li class="nav-item start <?php if($this->router->fetch_module() == "manage_teachers") { echo "active"; } ?>">
					<a href="<?php echo base_url('manage_teachers/admin'); ?>" class="nav-link nav-toggle" onClick = "showLoader();">
						<i class="icon-users"></i>
						<span class="title">Manage Teachers</span>
						<span class="selected"></span>
					</a> 
				</li>
				<li class="nav-item start <?php if($this->router->fetch_module() == "manage_subjects") { echo "active"; } ?>">
					<a href="<?php echo base_url('manage_subjects/admin'); ?>" class="nav-link nav-toggle" onClick = "showLoader();">
						<i class="icon-notebook"></i>
						<span class="title">Manage Subjects</span>
						<span class="selected"></span>
					</a> 
				</li>
				<li class="nav-item start <?php if($this->router->fetch_module() == "manage_classes") { echo "active"; } ?>">
					<a href="<?php echo base_url('manage_classes/admin'); ?>" class="nav-link nav-toggle" onClick = "showLoader();">
						<i class="icon-book-open"></i>
						<span class="title">Manage Classes</span>
						<span class="selected"></span>
					</a> 
				</li>
				<li class="nav-item start <?php if($this->router->fetch_module() == "time_table") { echo "active"; } ?>">
					<a href="<?php echo base_url('time_table/admin'); ?>" class="nav-link nav-toggle" onClick = "showLoader();">
						<i class="icon-clock"></i> 
						<span class="title">Time Table</span>
						<span class="selected"></span>
					</a> 
				</li>
				<li class="nav-item start <?php if($this->router->fetch_module() == "manage_notice_board") { echo "active"; } ?>">
					<a href="<?php echo base_url('manage_notice_board/admin'); ?>" class="nav-link nav-toggle" onClick = "showLoader();">
						<i class="icon-wrench"></i>
						<span class="title">Manage Notice Board</span>
						<span class="selected"></span>
					</a> 
				</li>
				<li class="nav-item start <?php if($this->router->fetch_module() == "settings") { echo "active"; } ?>">
					<a href="<?php echo base_url('settings/admin'); ?>" class="nav-link nav-toggle" onClick = "showLoader();">
						<i class="icon-wrench"></i>
						<span class="title">Settings</span>
						<span class="selected"></span>
					</a> 
				</li>
			</ul>
			<!-- END SIDEBAR MENU -->
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
	</div>
	 <!-- END SIDEBAR -->