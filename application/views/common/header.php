<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Paramount</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
		<link rel="icon" href="<?php echo base_url('assets/front/images/favicon.png')?>" sizes="16x16" type="image/png">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/admin/'); ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/admin/'); ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/admin/'); ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/admin/'); ?>global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/admin/'); ?>global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url('assets/admin/'); ?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/admin/'); ?>global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/admin/'); ?>global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url('assets/admin/'); ?>global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        
		<link href="<?php echo base_url('assets/admin/'); ?>global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url('assets/admin/'); ?>layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/admin/'); ?>layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url('assets/admin/'); ?>css/custom.css" rel="stylesheet" type="text/css" />

        <!--- Button spinner  --->
        <link href="<?php echo base_url('assets/admin/'); ?>global/plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
       
	    <!--- Button spinner  --->
        <link href="<?php echo base_url('assets/admin/css/style.css'); ?>" rel="stylesheet" type="text/css" />
		
		
		<!-- Datatable -->
		<link href="<?php echo base_url('assets/admin/'); ?>global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/admin/'); ?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
		
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
		<input type = "hidden" id = "base_url" value = "<?php echo base_url(); ?>">
		<div id="on_load_loader" class="hide"></div>
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="">
							<img style="margin: 6px 0 0;width: 40px;" src="<?php echo base_url('assets/admin/'); ?>images/logo.png" alt="logo" class="logo-default" />
						</a>
						<div class="menu-toggler sidebar-toggler">
							<span></span>
						</div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <?php $uavatart = $this->common_model->GetUserAvatar(); ?>
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <!--
										<img alt="" style="width: 32px;background: #fff;height: 32px;" class="img-circle" src="<?php echo $uavatart; ?>" />
                                    -->
									<span class="username username-hide-on-mobile"> <?php echo  ucfirst($this->session->userdata('admin_name')); ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
									<li>
                                        <a href="<?php echo base_url('profile/admin'); ?>">
										<i class="icon-user"></i> Profile</a>
                                    <li>
                                        <a href="<?php echo base_url('home/logout') ?>">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
			<!-- END HEADER -->
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
			<?php $this->load->view('common/sidebar'); ?>