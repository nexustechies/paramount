<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
	}
	
	/* Function used to collect email from subscription form */
	function subscribe(){
		$email = $this->input->post('email');
		
		if(!filter_var($email, FILTER_VALIDATE_EMAIL))
		{
			$json_data['type'] = 'alert';
			$json_data['status'] = 'error';
			$json_data['html'] = 'Please enter a valid email.';
			echo json_encode($json_data);
			die();
		}
		
		$condition = array('email' => $email);
		$key_exist = $this->common_model->checkExists(SUBSCRIPTIONS_TABLE,$condition);
		// already subscribed
		if($key_exist)
		{
			$json_data['type'] = 'alert';
			$json_data['status'] = 'error';
			$json_data['html'] = 'This email is already exists in our system.';
			echo json_encode($json_data);
			die();
		}
		$ip =$this->input->ip_address();
		$data = array('email' => $email , 'ip' => $ip, 'timestamp' => time() );
		$insert = $this->common_model->InsertTableData(SUBSCRIPTIONS_TABLE,$data);
		if($insert)
		{
			$json_data['type'] = 'alert';
			$json_data['status'] = 'success';
			$json_data['html'] = 'Successfully subscribed to our mailing list.';
		}
		else
		{
			$json_data['type'] = 'alert';
			$json_data['status'] = 'error';
			$json_data['html'] = 'Something went wrong please try again.';
		}
		echo json_encode($json_data);
	}
	/* Function used to send contact email */
	function contact_form()
	{
		$name = strip_tags($this->input->post('name')); 
		$email = strip_tags($this->input->post('email'));
		//$phone = strip_tags($this->input->post('phone'));
		$subject = strip_tags($this->input->post('subject'));
		$message = strip_tags($this->input->post('message'));
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean');
		$this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');

		if($this->form_validation->run() == true) 
		{
			$html = '<div>
					<p>
						<b>Name</b><br />
						<span>' . $name . '</span>
					</p>
					<p>
						<b>E-mail</b><br />
						<span>' . $email . '</span>
					</p>
					
					<p>
						<b>Subject</b><br />
						<span>' . $subject . '</span>
					</p>
					<p>
						<b>Message</b><br />
						<span>' . $message . '</span>
					</p>
				</div>';
			$headers = array(
				"From: Paramount <".EMAIL_SENDER.">",
				"Reply-To: " . $email . "",
				"MIME-Version: 1.0",
				"Content-type:text/html;charset=UTF-8",
				"X-Mailer: PHP/" . PHP_VERSION
			);
			$headers = implode("\r\n", $headers);
			
			if (mail(ADMIN_EMAIL, "Contact US Enquiry", $html, $headers))
			{
				$json_data['class'] = 'success';
				$json_data['html'] = "Thanks for contacting us. We will get back to you soon.";
			}
			else
			{
				$json_data['status'] = 'error';
				$json_data['html'] = "Email not sent please try again later.";
			}
		}
		else
		{
			$json_data['type'] = 'alert';
			$json_data['status'] = 'error';
			$json_data['html'] = validation_errors();
		}
		echo json_encode($json_data);
	}
    
}