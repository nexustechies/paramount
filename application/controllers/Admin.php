<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library('user_agent');
    } 
	
	public function index()
	{
		$page_data['title'] = "Admin Login";
		$this->load->view('login/login',$page_data);
	}

	public function doLogin() // admin login process;
	{
		$refer =  $this->agent->referrer();
		$email = $this->input->post('email',TRUE);
		$password = $this->input->post('password');
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if ($this->form_validation->run() == TRUE)
        {
			$table = ADMIN_TABLE;
        	$result = $this->common_model->GetSingleRow($table,array('email' => $email, 'password' => MD5($password)))  ;
			if(isset($result))
			{
				$sess_array = array( 'admin_id' => $result->id, 'admin_name' => $result->name,'admin_email' => $result->email, );
				$this->session->set_userdata($sess_array);
				redirect('dashboard/admin');
			}
			else
			{
				$this->session->set_flashdata('flash_message','Invalid Username or Password.');
				$this->session->set_flashdata('class','danger');
				redirect($refer);
			}
		}
		else
		{
			$this->form_validation->set_error_delimiters('', '');
			$this->session->set_flashdata('flash_message',validation_errors());
			$this->session->set_flashdata('class','danger');
			redirect($refer);
		}
	}
	
	public function view_profile($id)
	{
		$this->common_model->CheckAdminSession(); /// secure login 
		$page_data = (array)$this->common_model->GetSingleRow(USERS_TABLE,array('id' => $id));
		$user_meta = $this->common_model->get_user_meta($id);
		$user_meta = array_merge($page_data,$user_meta);
		
		/* _p($user_meta);
		die(); */
		
		$languages = $user_meta['languages'];
		$seniority_ids = $user_meta['seniority_ids'];
		$speciality_ids = $user_meta['speciality_ids'];
		$ahpr_ids = $user_meta['ahpr_ids'];
		$user_meta['languages'] = $this->common_model->get_coma_values(LANGUAGES_TABLE,$languages);
		$user_meta['seniority'] = $this->common_model->get_coma_values(CANDIDATE_SENIORITY_TABLE,$seniority_ids);
		$user_meta['speciality'] = $this->common_model->get_coma_values(CANDIDATE_SPECIALITIES_TABLE,$speciality_ids);
		$user_meta['ahpr'] = $this->common_model->get_coma_values(AUSTRALIAN_HEALTH_TABLE,$ahpr_ids);
		$user_meta['experience'] = $this->common_model->GetSingleValue(EXPERIENCES_TABLE,'title', array('id' => $user_meta['experience']));
		$user_meta['highest_education'] = $this->common_model->GetSingleValue(HIGHEST_EDUCATIONS_TABLE,'title', array('id' => $user_meta['highest_education']));
		$user_meta['australian_residency'] =   $this->common_model->GetSingleValue(AUSTRALIAN_RESIDENCY_TABLE,'title', array('id' => $user_meta['australian_residency']));
		$user_meta['career_status'] =   $this->common_model->GetSingleValue(CAREER_STATUS,'title', array('id' => $user_meta['career_status']));
		
		$profile_pic = base_url('assets/front/images/placeholder.png');
		if(!empty($user_meta['profile_image']) && is_file(FCPATH.'/uploads/'.$user_meta['profile_image']) )
		{
			$profile_pic = base_url('uploads/'.$user_meta['profile_image']);
		}
		$user_meta['profile_image'] = $profile_pic;
		
		$header_data['active_menu'] = "dashboard";
		$this->load->view('front/common/header',$header_data);
		$this->load->view('common/resume', $user_meta);
		$this->load->view('front/common/footer');
	}
}
