<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	function __construct()
 	{
  		parent::__construct();
		$this->common_model->CheckAdminSession(); /// secure login 
	}
	
	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('common/dashboards/admin');
		$this->load->view('common/footer');
	}
	
	public function logout()
	{
		$this->session->unset_userdata('admin_id');
		redirect(base_url(), 'refresh' );
	}
}