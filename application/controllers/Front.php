<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		
	}
	public function index()
	{
		
		$this->load->view('front/layout');
	}
	
	public function home()
	{
		
		$this->load->view('front/index');
	}
	
	public function get_staff()
	{
		$results = $this->common_model->GetTableRows(TEACHERS_TABLE, array('status' => 1), array('id','desc'));
		$teachers = [];
		foreach($results as $key => $result)
		{
			$subject_name = [];
			$subjects = explode(',',$result['subjects']);
			foreach($subjects as $subject)
			{
				$subject_n = $this->common_model->GetSingleValue(SUBJECTS_TABLE,'title',array('id' => $subject));
				array_push($subject_name,$subject_n);
			}
			
			$prfile_img = base_url('assets/front/img/avtar.png');
			if(!empty($result['profile_image'])):
				$prfile_img = base_url('uploads/'.$result['profile_image']);
			endif;
			$teachers[$key] = $result;
			$teachers[$key]['profile_image'] = $prfile_img;
			$teachers[$key]['subjects'] = implode(', ',$subject_name) ;
		}
		
		echo json_encode($teachers);
	}
	
	public function about_us()
	{
		$page_data =[];
		$this->load->view('front/about', $page_data);
	}
	
	public function contact_us() 
	{
		$page_data['contact_us'] = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'contact_us'));
		$this->load->view('front/contact_us', $page_data);
	}
	public function get_contact_us() 
	{
		$results = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'contact_us'));
		$social_link = unserialize($results);		
		echo json_encode($social_link);
	}
	public function get_social_links() 
	{
		$results = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'social_links'));
		$social_links = unserialize($results);		
		echo json_encode($social_links);
	}
	 
	public function time_table()
	{
		$page_data =[];
		$page_data['results'] = $this->common_model->GetTableRows(TIME_TABLE, array('status' => 1) ,array('id','desc'));
		$this->load->view('front/timetable', $page_data);
	}
	
	
	public function get_timetable()
	{
		$results = $this->common_model->GetTableRows(TIME_TABLE,array('status' => 1) ,array('id','desc'));
		$html =$title ='';
		foreach($results as $key => $result):
			$title = $result['title'];
			$data = unserialize($result['time_table']);
			$shifts = [];
			foreach($data as $timetable):
				$shifts[$timetable['shift']][]=$timetable;
			endforeach;
			foreach($shifts as $shift => $timetable):
				if($shift=="2"):
					$html .= '<tr><td colspan="9"><h3> Evening shift</h3></td></tr>';
				endif;
				$html .= self::get_timetablehtml($timetable);
			endforeach;
		endforeach;
		$json_data['html'] = $html;
		$json_data['title'] = $title;
		$json_data['status'] = 200;
		echo json_encode($json_data);
	}
	
	public function get_timetablehtml($array)
	{
		$location_array = array('1' => "Hall 1", '2' => 'Hall 2', '3' => 'Room 1' , '4' => "Room 2");
		$html = '';
		foreach($array as $timetable):
				$subject = $this->common_model->getSingleValue(SUBJECTS_TABLE,'title',array('id' => $timetable['subjects']));
				$class = $this->common_model->getSingleValue(CLASSES_TABLE,'title',array('id' => $timetable['class']));
				$teacher = $this->common_model->getSingleValue(TEACHERS_TABLE,'name',array('id' => $timetable['teacher']));
				$html .= '<tr>';
					$html .='<tr>
								<td rowspan="2" class="tym">'.$timetable['from'].'-'.$timetable['to'].'</td>';
						foreach($location_array as $location_key => $location):
							if($location_key == $timetable['room']):
								$html .= '<td >'.$subject.'</td>
										  <td >'.$class.'</td>';
							else:
								$html .='<td colspan="2"></td>';
							endif;
						endforeach;
					$html .='</tr>'; 
					$html .='<tr>';
						foreach($location_array as $location_key => $location):
							if($location_key == $timetable['room']):
								$html .= '<td colspan="2">'.$teacher.'</td>';
							else:
								$html .='<td colspan="2"></td>';
							endif;
						endforeach;
					$html .='</tr>'; 
				$html .= '</tr>';
			endforeach;
		return $html;	
	}
	
	public function staff()
	{
		$this->load->view('front/staff');
	}
	public function notice_board()
	{
		$this->load->view('front/notice_board');
	}
	public function get_noticeboard_data()
	{
		$json_data['coaching_institite'] = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'coaching_institite'));
		$json_data['faq'] = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'faq'));
		$pdf = $this->common_model->GetSingleValue(SETTINGS_TABLE,'value',array('type' => 'pdf'));
		$json_data['pdf'] = base_url('pdf/'.$pdf);
		
		echo json_encode($json_data);
	}
	
	public function faq()
	{
		$page_data =[];
		$this->load->view('front/common/header');
		$this->load->view('front/faq');
		$this->load->view('front/common/footer');
	}
	
	public function register()
	{
		
		//$_POST= json_decode(file_get_contents('php://input'), TRUE);
		if($this->input->post('s')) // if form submitted
		{
			$name = strip_tags($this->input->post('name'));
			$email = strip_tags($this->input->post('email'));
			$dob = strip_tags($this->input->post('dob'));
			$phone = strip_tags($this->input->post('phone'));
			$father_phone = strip_tags($this->input->post('father_phone'));
			$father_occupation = strip_tags($this->input->post('father_occupation'));
			$address = strip_tags($this->input->post('address'));
			$school = strip_tags($this->input->post('school'));
			$subjects = strip_tags($this->input->post('subjects'));

			$classes = strip_tags($this->input->post('classes')); 
			
			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
			$this->form_validation->set_rules('dob', 'Date Of Birth', 'trim|required|xss_clean');
			$this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean');
			$this->form_validation->set_rules('father_phone', 'Father Phone', 'trim|required|xss_clean');
			$this->form_validation->set_rules('address', 'Address', 'trim|required|xss_clean');
			$this->form_validation->set_rules('school', 'School', 'trim|required|xss_clean');
			$this->form_validation->set_error_delimiters('<span>','</span><br>');
			if ($this->form_validation->run() == true)
			{
				
				$profile_image = self::upload_profile_image();
				
				$data = array
						(
							'name' =>  $name,
							'email' =>  $email,
							'dob' =>  $dob,
							'phone' =>  $phone,
							'profile_image' =>  $profile_image,
							'father_phone' =>  $father_phone,
							'father_occupation' =>  $father_occupation,
							'address' =>  $address,
							'school' =>  $school, 
							'subjects' =>  $subjects,
							'class' =>  $classes,
							'created_on' =>  time(),
						);
						
				$insert_id = $this->common_model->InsertTableData(STUDENTS_TABLE,$data);
				if($insert_id)
				{
					$json_data['class'] = 'success';
					$json_data['html'] = 'Student is successfully registered.';
				}
				else
				{
					$json_data['status'] = 'error';
					$json_data['html'] = 'Something went wrong please try again.';
				}
			}
			else
			{
				$json_data['status'] = 'error';
				$json_data['html'] = validation_errors();
			}
			echo json_encode($json_data);
			die();
		}
		
		$page_data['subjects'] = $this->common_model->SelectDropdown(SUBJECTS_TABLE,'title','id',array($this->input->post('subjects')),array('status' => 1));
		$page_data['classes'] = $this->common_model->SelectDropdown(CLASSES_TABLE,'title','id',array($this->input->post('classes')));
		$this->load->view('front/register',$page_data);
	}
	
	function upload_profile_image()
	{

		$profile_image = "";
		if(isset($_FILES['profile_image']['name']) && !empty($_FILES['profile_image']['name']))
		{
			$name = $_FILES['profile_image']['name'];
			$upload_dir = dirname(APPPATH)."/uploads";
			$t = explode(".", $name);
			$ext = end($t);

			$file_info = pathinfo($name);
			$file_name = $file_info['filename'];
			$replaced_name = preg_replace("/[^a-zA-Z]+/", "", $file_name);
			$name = $replaced_name.'.'.$file_info['extension'];
			
			$config['upload_path'] = dirname(APPPATH)."/uploads";
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['max_size'] = "20048";
			
			$config['file_name'] = 'img_'.time().'_'.$name;
			$this->load->library("upload", $config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('profile_image'))
			{
				$profile_image = $config['file_name'];
			}
		}
		return $profile_image;
	}
    
}