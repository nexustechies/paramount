<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/* 
	Design & Developed by Nexus Techies 
	Author: Subhash Chand
	Email: provider.nexus@gmail.com

	*/
    function check_isset($key,$array)
	{
		$value = "";
		if(isset($array[$key]))
		{
			$value = $array[$key];
		}
		return $value;
	}
	
	function check_checkbox($first,$second)
	{
		$checked = "";
		if($first == $second)
		{
			$checked = "checked";
		}
		return $checked;
	}
	function check_selected($first,$second)
	{
		$selected = "";
		if($first == $second)
		{
			$selected = "selected";
		}
		return $selected;
	}
	
	/* clean url  */
	
	function clean($string) 
	{
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

	   return preg_replace('/-+/', '-', strtolower($string)); // Replaces multiple hyphens with single one.
	}
	
	function isLoggedin()
	{
		$in = FALSE;
		$obj =& get_instance();
		if($obj->session->userdata('user_id'))
		{ 
			$in = TRUE;
		}
		return $in;
	}
	
	/* Get currency code */
	function get_currency_code($currency="aed")
	{
		$currency_code = "";
		if($currency="aed")
		{
			$currency_code = "&#x62f;&#x2e;&#x625;";
		}
		return $currency_code;
	}
	
	function _p($array)
	{
		echo "<pre>";
		print_r($array);
		echo "</pre>";
		
	}
	
	/* Time ago  */
	function Timeago( $time )
	{
		$time_difference = time() - $time;

		if( $time_difference < 1 ) { return 'less than 1 second ago'; }
		$condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
					30 * 24 * 60 * 60       =>  'month',
					24 * 60 * 60            =>  'day',
					60 * 60                 =>  'hour',
					60                      =>  'minute',
					1                       =>  'second'
		);

		foreach( $condition as $secs => $str )
		{
			$d = $time_difference / $secs;

			if( $d >= 1 )
			{
				$t = round( $d );
				return  $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
			}
		}
	}
	
	function show_flash_message()
	{
		$obj =& get_instance();
		if($obj->session->flashdata('flash_message'))
		{ 
			$title_text = $obj->session->flashdata('class') == "success" ? "Success" : "Error";
			?>
			<div class="alert alert-<?php echo $obj->session->flashdata('class'); ?> alert-dismissible fade show" role="alert">
				<strong><?php echo $title_text; ?>!</strong> <?php echo $obj->session->flashdata('flash_message'); ?> 
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php
		}
	}
	
	/* Get file mime type */
	function get_file_type($filename) {
		if(empty($filename))
		{
			return "";
		}
		$mime_types = array(

			'txt' => 'text/plain',
			'htm' => 'text/html',
			'html' => 'text/html',
			'php' => 'text/html',
			'css' => 'text/css',
			'js' => 'application/javascript',
			'json' => 'application/json',
			'xml' => 'application/xml',
			'swf' => 'application/x-shockwave-flash',
			'flv' => 'video/x-flv',

			// images
			'png' => 'image/png',
			'jpe' => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'jpg' => 'image/jpeg',
			'gif' => 'image/gif',
			'bmp' => 'image/bmp',
			'ico' => 'image/vnd.microsoft.icon',
			'tiff' => 'image/tiff',
			'tif' => 'image/tiff',
			'svg' => 'image/svg+xml',
			'svgz' => 'image/svg+xml',

			// archives
			'zip' => 'application/zip',
			'rar' => 'application/x-rar-compressed',
			'exe' => 'application/x-msdownload',
			'msi' => 'application/x-msdownload',
			'cab' => 'application/vnd.ms-cab-compressed',

			// audio/video
			'mp3' => 'audio/mpeg',
			'qt' => 'video/quicktime',
			'mov' => 'video/quicktime',

			// adobe
			'pdf' => 'application/pdf',
			'psd' => 'image/vnd.adobe.photoshop',
			'ai' => 'application/postscript',
			'eps' => 'application/postscript',
			'ps' => 'application/postscript',

			// ms office
			'doc' => 'application/msword',
			'rtf' => 'application/rtf',
			'xls' => 'application/vnd.ms-excel',
			'ppt' => 'application/vnd.ms-powerpoint',

			// open office
			'odt' => 'application/vnd.oasis.opendocument.text',
			'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
		);

		$ext = strtolower(array_pop(explode('.',$filename)));
		if (array_key_exists($ext, $mime_types)) {
			return $mime_types[$ext];
		}
		elseif (function_exists('finfo_open')) {
			$finfo = finfo_open(FILEINFO_MIME);
			$mimetype = finfo_file($finfo, $filename);
			finfo_close($finfo);
			return $mimetype;
		}
		else {
			return 'application/octet-stream';
		}
	}
	
?>